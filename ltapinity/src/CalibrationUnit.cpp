#include "CalibrationUnit.h"

using namespace std;
using namespace tapinity;

CalibrationUnit::CalibrationUnit(){
    cameraToScreen_ = 0;
    screenUpperLeft.x=0.0f;
    screenUpperLeft.y=0.0f;
    screenLowerRight.x=1.0f;
    screenLowerRight.y=1.0f;

    width_ =DEFAULT_CAPTURE_WIDTH;
    height_ =DEFAULT_CAPTURE_HEIGHT;

    minCameraPoint.x=0.0f;
    minCameraPoint.y=0.0f;
    maxCameraPoint.x=static_cast<float>(width_);
    maxCameraPoint.y=static_cast<float>(height_);

    int i,j,k=0;

    for(j=0; j<CALIBRATIONX; j++){
        for(i=0; i<CALIBRATIONY;i++){
            triangles_[k] = i + j * CALIBRATIONX;
            triangles_[k+1] = (i+1) + j*CALIBRATIONX;
            triangles_[k+2] = (i+1)+(j+1)*CALIBRATIONX;
            k = k + 3;
            //lower right triangle
            triangles_[k] = i+j*CALIBRATIONX;
            triangles_[k+1] = i+(j+1)*CALIBRATIONX;
            triangles_[k+2] = (i+1)+(j+1)*CALIBRATIONX;
            k = k + 3;
        }
    }
    calibrating_=false;
    isCalibrated_=false;
    step_=0;
    image_=NULL;
    initCameraScreen();
    mapCameraToScreen();
}

CalibrationUnit::~CalibrationUnit(){
    for( int i = 0 ; i < width_+1; i++ ) {
        for(int j=0; j<height_+1;j++){
            delete [] cameraToScreen_[i][j];
        }
        delete [] cameraToScreen_[i];
    }
    //for(int i=0; i<width_+1;i++)
    delete [] cameraToScreen_;
}

void CalibrationUnit::begin(IplImage* image){
    image_=image;
    initCameraScreen();
    calibrating_=true;
    isCalibrated_=false;
    step_=0;
}

void CalibrationUnit::step(float x, float y){
    if(calibrating_){
        if(second_click){
            setCameraPoint(step_,x,y); 
            step_++;
            if(step_>=CALIBRATIONVERTICES){
                calibrating_=false;
                isCalibrated_=true;
                printf("Calibration done.");
                mapCameraToScreen();
            }
            second_click=false;
        }
        second_click=true;
    }
}

void CalibrationUnit::revert()
{
    if(calibrating_)
    {
        step_--;
        if(step_ < 0)
        {
            step_ = 0;
        }
    }
}

void CalibrationUnit::setCameraPoint(unsigned int no, float x, float y){
    CvPoint2D32f p;
    p.x=x;
    p.y=y;
    if(x>maxCameraPoint.x)
        maxCameraPoint.x=x;
    if(x<minCameraPoint.x)
        minCameraPoint.x=x;
    if(y>maxCameraPoint.y)
        maxCameraPoint.y=y;
    if(y<minCameraPoint.y)
        minCameraPoint.y=y;

    //if(no>=0 && no<CALIBRATIONVERTICES){
    camera_[no]=p;
    //cout<<"setting point ("<<no<<") = ("<< p.x << ","<<p.y <<")"<<endl;
    //}
}

void CalibrationUnit::mapCameraToScreen(){
    cameraToScreen_ = new float**[width_+1];
    for(int i=0; i<width_+1;i++)
        cameraToScreen_[i]=new float*[height_+1];
    for(int i=0; i<width_+1;i++)
        for(int j=0; j<height_+1; j++)
            cameraToScreen_[i][j]=new float[2];
    int k=0;
    for(int i=0; i< width_+1; i++){
        for(int j=0; j<height_+1; j++){
            float x=static_cast<float>(i);
            float y=static_cast<float>(j);
            calculateCameraToScreenPoint(x,y);
            cameraToScreen_[i][j][0]=x;
            cameraToScreen_[i][j][1]=y;
        }
    }
}

void CalibrationUnit::cameraToScreenPoint(float &x, float &y){
    if(x<0) x=0;
    if(y<0) y=0;
    if(x>width_) x=static_cast<float>(width_);
    if(y>height_) y=static_cast<float>(height_);

    int xx=static_cast<int>(x);
    int yy=static_cast<int>(y);
    x=cameraToScreen_[xx][yy][0];
    y=cameraToScreen_[xx][yy][1];
}

void CalibrationUnit::calculateCameraToScreenPoint(float &x, float &y){
    int triangle = assignToTriangle(x,y);
    if(triangle == -1){
        x = y = 0;
        return;
    }
    CvPoint2D32f p0 = camera_[triangles_[triangle]];
    CvPoint2D32f p1 = camera_[triangles_[triangle+1]];
    CvPoint2D32f p2 = camera_[triangles_[triangle+2]];

    CvPoint2D32f s0 = screen_[triangles_[triangle]];
    CvPoint2D32f s1 = screen_[triangles_[triangle+1]];
    CvPoint2D32f s2 = screen_[triangles_[triangle+2]];

    float triangle_area = (p0.x - p1.x)*(p0.y - p2.y) - (p0.y - p1.y)*(p0.x - p2.x);
    float p_p1_p2_area = (x-p1.x)*(y-p2.y) - (y-p1.y)*(x-p2.x);
    float p0_p_p2_area = (p0.x-x)*(p0.y-p2.y) - (p0.y-y)*(p0.x-p2.x);

    float area_0 = p_p1_p2_area/triangle_area;
    float area_1 = p0_p_p2_area/triangle_area;

    CvPoint2D32f newpoint;
    newpoint.x = s0.x*area_0+s1.x*area_1+s2.x*(1.0f-area_0-area_1);
    newpoint.y= s0.y*area_0+s1.y*area_1+s2.y*(1.0f-area_0-area_1);

    x=newpoint.x;
    y=newpoint.y;
}

void CalibrationUnit::cameraToScreenRect(float &width, float &height, float midX, float midY){
    float halfX = width * 0.5f;
    float halfY = height * 0.5f;
    float centerX = ((maxCameraPoint.x - minCameraPoint.x)/2) + minCameraPoint.x;
    float centerY = ((maxCameraPoint.y - minCameraPoint.y)/2) + minCameraPoint.y;
    float ulX = centerX - halfX;
    float ulY = centerY - halfY;
    float lrX = centerX + halfX;
    float lrY = centerY + halfY;
    cameraToScreenPoint(ulX, ulY);
    cameraToScreenPoint(lrX, lrY);
    width = std::fabs(lrX - ulX);
    height = std::fabs(ulY - lrY);
}

void CalibrationUnit::initCameraScreen(){
    int i,j,k=0;
    //SCREEN
    float xd=screenLowerRight.x-screenUpperLeft.x;
    float yd=screenLowerRight.y-screenUpperLeft.y;
    xd=xd/static_cast<float>(CALIBRATIONX);
    yd=yd/static_cast<float>(CALIBRATIONY);
    //cout<<"SCREEN"<<endl;
    for(i=0; i<CALIBRATIONX+1;i++){
        for(j=0; j<CALIBRATIONY+1;j++){
            CvPoint2D32f point;
            point.x=screenUpperLeft.x+xd*i;
            point.y=screenUpperLeft.y+yd*j;
            //cout<<"point ("<<i<<","<<j<<") = ("<< point.x << ","<<point.y <<")"<<endl;
            screen_[k++]=point;
        }
    }
    //CAMERA
    k=0;
    if(image_){
        width_ = image_->width;
        height_ = image_->height;
    }
    minCameraPoint.x=0;
    minCameraPoint.y=0;
    maxCameraPoint.x=static_cast<float>(width_);
    maxCameraPoint.y=static_cast<float>(height_);

    float FLOATX = static_cast<float>(CALIBRATIONX);
    float FLOATY = static_cast<float>(CALIBRATIONY);
    float width = static_cast<float>(width_);
    float height = static_cast<float>(height_);
    //cout<<"CAMERA"<<endl;
    for(i=0; i<CALIBRATIONX+1;i++){
        for(j=0; j<CALIBRATIONY+1;j++){
            CvPoint2D32f point;
            point.x=(i*width)/FLOATX;
            point.y=(j*height)/FLOATY;
            //cout<<"point ("<<i<<","<<j<<") = ("<< point.x << ","<<point.y <<")"<<endl;
            camera_[k++]=point;
        }
    }
}

int CalibrationUnit::assignToTriangle(float x, float y){
    for(int i=0; i<CALIBRATIONINDICES;i+=3){
        if(isInTriangle(x,y,camera_[triangles_[i]], camera_[triangles_[i+1]], camera_[triangles_[i+2]]))
            return i;
    }
    return -1;
}

float CalibrationUnit::side(float x, float y, CvPoint2D32f p2, CvPoint2D32f p3)
{
    CvPoint2D32f p1;
    p1.x=x;
    p1.y=y;
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool CalibrationUnit::isInTriangle(float x, float y, CvPoint2D32f v1, CvPoint2D32f v2, CvPoint2D32f v3)
{
    CvPoint2D32f pt;
    pt.x=x;
    pt.y=y;
    bool b1, b2, b3;

    b1 = side(pt.x,pt.y, v1, v2) < 0.0f;
    b2 = side(pt.x,pt.y, v2, v3) < 0.0f;
    b3 = side(pt.x,pt.y, v3, v1) < 0.0f;

    return ((b1 == b2) && (b2 == b3));
}

