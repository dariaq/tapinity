#include "TWindow.h"

namespace tapinity {
    namespace ui {
        TWindow::TWindow(){}
        TWindow::~TWindow(){}

        void TWindow::touchDown(TEvent* e){
            TWidget* tw = hitTest(e->data_);
            events_.insert(std::make_pair(e->data_.ID, tw));
            tw->getGestureManager()->touchDown(e);
            tw->onTouchDown(e);
        }

        void TWindow::touchMove(TEvent* e){
            TWidget* tw = events_.at(e->data_.ID);
            tw->getGestureManager()->touchMove(e);
            tw->onTouchMove(e);
        }

        void TWindow::touchUp(TEvent* e){
            TWidget* tw = events_.at(e->data_.ID);
            tw->getGestureManager()->touchUp(e);
            tw->onTouchUp(e);
            events_.erase(e->data_.ID);
        }
    }
}
