#include "GestureManager.h"

namespace tapinity {
    namespace gestures {

        GestureManager::GestureManager(){
            gesture_ = NULL;
            prev_gesture_ = NULL;
        }

        GestureManager::~GestureManager(){
            delete gesture_;
            delete prev_gesture_;

            for(unsigned int i = 0; i < gesture_recognizers_.size(); ++i)
                delete gesture_recognizers_[i];
            gesture_recognizers_.clear();
        }

        void GestureManager::swapGestures(){
            TGesture* tmp = prev_gesture_;
            gesture_->time_stamp_end_ = clock();
            prev_gesture_ = gesture_;
            gesture_ = NULL;        
            delete tmp;
        }

        void GestureManager::addGestureRecognizer(TGestureType type, GestureRecognizer* gr){
            gesture_recognizers_.push_back(gr); 
        }   

        void GestureManager::touchDown(TEvent* e){
            if(!gesture_){
                gesture_ = new TGesture(e->data_);
            }

            gesture_->pushFinger(e->data_);

            if(gesture_->getFingers() == 2){
                gesture_->angle_ = GestureUtils::calcAngle(gesture_);
                gesture_->scale_ = GestureUtils::calcScale(gesture_);
            }

            for(unsigned int i = 0; i < gesture_recognizers_.size(); ++i){
                if(e->cancelled_) break;
                gesture_recognizers_[i]->detectGesture(e, gesture_, prev_gesture_);
            }
        }

        void GestureManager::touchMove(TEvent* e){
            gesture_->replaceFinger(e->data_);

            for(unsigned int i = 0; i < gesture_recognizers_.size(); ++i){
                if(e->cancelled_) break;
                gesture_recognizers_[i]->detectGesture(e, gesture_, prev_gesture_);
            }
        }

        void GestureManager::touchUp(TEvent* e){
            for(unsigned int i = 0; i < gesture_recognizers_.size(); ++i){
                if(e->cancelled_) break;
                gesture_recognizers_[i]->detectGesture(e, gesture_, prev_gesture_);
            }

            if(gesture_->getFingers() > 1){
                gesture_->popFinger(e->data_);
            } else {
                swapGestures();
            }
        }

    }
}
