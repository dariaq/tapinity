#include "Finger.h"

using namespace tapinity::blobs;

Finger::Finger() : age_(0){}

Finger::Finger(Blob b, int age, CvScalar color, bool active, int id){
    track_.push_back(b);
    age_=age;
    color_=color;
    active_=active;
    ID_=id;
}

Finger::~Finger(){}

void Finger::addPosition(Blob b){
    track_.push_back(b);
    if(track_.size()>age_)
        track_.erase(track_.begin());
}

Blob Finger::getLastBlob(){
    return track_[track_.size()-1];
}

CvPoint2D32f Finger::getDelta(){
    CvPoint2D32f p;
    if(track_.size()>1){
        p.x=track_[track_.size()-1].centroid_.x-track_[track_.size()-2].centroid_.x;
        p.y=track_[track_.size()-1].centroid_.y-track_[track_.size()-2].centroid_.y;
    }
    else{
        p.x=p.y=0;
    }
    return p;
}

TData Finger::getTouchData(){
    TData data;
    Blob blob=getLastBlob();
    data.x=blob.centroid_.x;
    data.y=blob.centroid_.y;
    data.h=blob.rect_.size.height;
    data.w=blob.rect_.size.width;
    data.a=blob.area_;
    CvPoint2D32f delta=getDelta();
    data.dx=delta.x;
    data.dy=delta.y;
    data.ID=ID_;
    return data;
}