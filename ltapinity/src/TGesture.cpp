#include "TGesture.h"

namespace tapinity{
    namespace gestures{

        TGesture::TGesture() {
            time_stamp_start_ = clock();
            type_ = NONE;
        }

        TGesture::TGesture(TData data) {
            time_stamp_start_ = clock();
            type_ = NONE;
        }

        TGesture::~TGesture() {};

        void TGesture::pushFinger(TData data) { 
            TGestureData td;
            td.x = data.x;
            td.y = data.y;
            fingers_.push_back(data); 
            init_data_.push_back(td);
        }

        void TGesture::popFinger(TData data) { 
            unsigned int i;
            for(i=0; i<fingers_.size(); ++i){
                if(fingers_[i].ID == data.ID)
                    break;
            }

            fingers_.erase(fingers_.begin()+i); 
            init_data_.erase(init_data_.begin()+i); 
        }

        void TGesture::replaceFinger(TData data) { 
            unsigned int i;
            for(i=0; i<fingers_.size(); ++i){
                if(fingers_[i].ID == data.ID){
                    fingers_[i] = data; 
                    break;
                }
            }
        }   
    }
}