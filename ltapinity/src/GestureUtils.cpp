#include "GestureUtils.h"

namespace tapinity {
    namespace gestures {

        double GestureUtils::calcAngle(TGesture* gesture_){
            TData a = gesture_->getTData(0);
            TData b = gesture_->getTData(1);
            TGestureData sa = gesture_->init_data_[0];
            TGestureData sb = gesture_->init_data_[1];
            float vx = b.x-a.x; float vy = b.y-a.y;
            float svx = sb.x-sa.x; float svy = sb.y-sa.y;
            return (double)((atan2(vy, vx) - atan2(svy, svx))*(DEG_TO_RAD));
        }

        float GestureUtils::calcDistance(TGesture* gesture_){
            TData a = gesture_->getTData(0);
            TData b = gesture_->getTData(1);
            float dist = sqrt((float)(b.x - a.x)*(float)(b.x - a.x) + (float)(b.y - a.y)*(float)(b.y - a.y))*-1;
            return dist;
        }

        float GestureUtils::calcScale(TGesture* gesture_){
            TData a = gesture_->getTData(0);
            TData b = gesture_->getTData(1);
            TGestureData sa = gesture_->init_data_[0];
            TGestureData sb = gesture_->init_data_[1];
            float sd = calcDistance(sa.x, sa.y, sb.x, sb.y);
            float ed = calcDistance(a.x, a.y, b.x, b.y);
            if(sd != 0){
                return (float) ed / sd;
            }
            return 1;
        }

        float GestureUtils::calcDistance(float px, float py, float qx, float qy){
            float x = qx - px;
            float y = qy - py;
            return sqrt((x * x) + (y * y));
        }

        double GestureUtils::calcAngle(float px, float py, float qx, float qy){
            double ret = atan2(qy - py, qx - px)*(DEG_TO_RAD);
            return ret;
        }
    }
}
