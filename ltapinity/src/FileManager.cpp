#include <iostream>
#include "FileManager.h"

using namespace cv;
using namespace tapinity;
using namespace tapinity::filters;
using namespace tapinity::blobs;

Camera* FileManager::readConfig(string filename, FilterList* list, ContoursFinder* finder, CalibrationUnit* calibration){
    FileStorage fs(filename, FileStorage::READ);
    FileNode n=fs["filters"];
    string parameters[][2] = {{"",""}, {"",""}, {"",""}, {"aperture",""}, {"level",""}, {"threshold","invert"}};
    unsigned int filters_size = list->size();
    for(unsigned int i = 0; i < filters_size; ++i){
        TFilter* f=&(list->operator[](i));
        string name = f->getName();
        FileNode filter=n[name];
        int active = filter["active"];
        cout<<name<<" "<<active<<endl;
        f->setActive(static_cast<bool>(active));
        for(int j=0; j<2; j++){
            if(parameters[i][j].length()>0){
                int val=static_cast<int>(filter[parameters[i][j]]);
                cout<<parameters[i][j]<<" = "<<val<<endl;
                f->setParameter(parameters[i][j],val);
            }
        }
    }
    cout<<"filters settings set"<<endl;
    n=fs["camera"];
    string ps3eye = "PS3Eye Camera";
    int id=n["ID"];
    string camn=n["camName"];
    Camera* tmp=0;
    if(!camn.compare(ps3eye)){
        tmp = new PS3CameraCapture(0);
        if(!tmp->isCamera())
            return NULL;
        FileNode cam=n["settings"];
        PS3Settings settings;
        settings.gain=static_cast<int>(cam["gain"]);
        settings.exposure=static_cast<int>(cam["exposure"]);
        settings.auto_gain=static_cast<int>(cam["auto_gain"]) != 0;
        settings.auto_exposure=static_cast<int>(cam["auto_exposure"]) != 0;
        (dynamic_cast<PS3CameraCapture*>(tmp))->setSettings(settings);
        cout<<"camera settings set"<<endl;
    }
    else
        tmp= new CameraCapture(id,camn);
    if(tmp == 0)
        return NULL;
    n=fs["blobs"];
    finder->setMinSize(n["min size"]);
    finder->setMaxSize(n["max size"]);
    cout<<"blobs settings set"<<endl;
    n=fs["calibration points"];
    FileNodeIterator it = n.begin(), it_end = n.end();
    int idx = 0;
    for( ; it != it_end; ++it, idx++ )
    {
        cout <<"i= "<<(int)(*it)["i"]<<" x=" << (float)(*it)["x"] << " y=" << (float)(*it)["y"] <<endl;
        CvPoint2D32f point;
        point.x=(float)(*it)["x"];
        point.y=(float)(*it)["y"];
        calibration->setCameraPoint(point,(int)(*it)["i"]);
    }
    cout<<"calibration points loaded"<<endl;
    calibration->setIsCalibrated(true);
    calibration->mapCameraToScreen();
    fs.release();
    return tmp;
}

void FileManager::saveConfig(string filename, FilterList* list, Camera* camera, ContoursFinder* finder, CalibrationUnit* calibration){
    FileStorage fs(filename, FileStorage::WRITE);
    fs<<"filters"<<"{";
    string parameters[][2] = {{"",""}, {"",""}, {"",""}, {"aperture",""}, {"level",""}, {"threshold","invert"}};
    unsigned int filters_size = list->size();
    for(unsigned int i = 0; i < filters_size; ++i){
        TFilter* f=&(list->operator[](i));
        string name = f->getName();
        fs<<name<<"{";
        fs<<"active"<<f->isActive();
        if(parameters[i][0].length() > 0){ 
            for(int j=0; j<2; j++)
                if(parameters[i][j].length() >0)
                    fs<<parameters[i][j]<<f->getParameter(parameters[i][j]);
        }
        fs<<"}";
    }
    fs<<"}"<<"camera"<<"{";
    fs<<"ID"<<camera->getID();
    fs<<"camName"<<camera->getName();
    fs<<"settings"<<"{";
    if(camera->isPS3Camera()){
        PS3Settings settings=(dynamic_cast<PS3CameraCapture*>(camera))->getSettings();
        //fs<<"{";
        fs<<"gain"<<settings.gain;
        fs<<"exposure"<<settings.exposure;
        fs<<"auto_gain"<<settings.auto_gain;
        fs<<"auto_exposure"<<settings.auto_exposure;
        //fs<<"}";
    }
    fs<<"}";
    fs<<"}"<<"blobs"<<"{";
    fs<<"min size"<<finder->getMinSize()<<"max size"<<finder->getMaxSize();
    fs<<"}";
    cout<<"filters and camera settings saved to "<<filename<<endl;
    fs<<"calibration points"<<"[";
    CvPoint2D32f* points=calibration->getCameraPoints();
    for(int i=0; i<CALIBRATIONVERTICES; i++){
        //fs<<"{:"<<"i"<<i<<"x"<<points[i].x<<"y"<<points[i].y<<"}";
        fs<<"{:"<<"i"<<i<<"x"<<calibration->getCameraPoint(i).x<<"y"<<calibration->getCameraPoint(i).y<<"}";
    }
    fs<<"]";
}
