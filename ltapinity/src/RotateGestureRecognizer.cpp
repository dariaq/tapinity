#include "RotateGestureRecognizer.h"

namespace tapinity {
    namespace gestures {
        void RotateGestureRecognizer::detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_){
            if(e->type_ != TOUCHMOVE || gesture_->getFingers() != 2 || gesture_->type_ == DRAG)
                return;

            double angle = GestureUtils::calcAngle(gesture_);
            double delta = angle - gesture_->angle_;
            if( abs(delta) > ROTATE_DELTA ||
                gesture_->type_ == ROTATE ){
                    float x = gesture_->getTData(0).x;
                    float y = gesture_->getTData(0).y;

                    gesture_->angle_ = angle;
                    gesture_->data_.angle = delta;
                    gesture_->type_ = ROTATE;
                    gesture_->data_.x = x;
                    gesture_->data_.y = y;
                    gesture_->data_.dx = 0;
                    gesture_->data_.dy = 0;

                    notifyRotate(gesture_);
                    e->cancel();
            }
        }

        void RotateGestureRecognizer::notifyRotate(TGesture* gesture){
            for (unsigned int i = 0; i < listeners_.size(); ++i) {
                ((RotateGestureListener*)listeners_[i])->rotate(gesture);
            }
        }

    }
}