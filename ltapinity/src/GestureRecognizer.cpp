#include "GestureRecognizer.h"

namespace tapinity {
    namespace gestures {
        const float GestureRecognizer::DIST_DELTA = 0.05f;

        const float GestureRecognizer::DRAG_EPS = 0.005f;
        const float GestureRecognizer::DRAG_DELTA = 0.5f;

        const float GestureRecognizer::ROTATE_EPS = 0.5f;
        const float GestureRecognizer::ROTATE_DELTA = 1.0f;

        const float GestureRecognizer::PINCH_EPS = 0.005f;
        const float GestureRecognizer::PINCH_DELTA = 0.1f;

        void GestureRecognizer::addGestureListener(TGestureListener* tgl){
            listeners_.push_back(tgl);
        }
    }
}

