#include <cv.h>
#include <highgui.h>

#include "TFilter.h"

using tapinity::filters::TFilter;

TFilter::TFilter(){
    name_ = "filter";
    source_=NULL;
    destination_=NULL;
    active_ = true;
}

TFilter::TFilter(string name){
    name_ = string(name);
    source_= NULL;
    destination_=NULL;
    active_ = true;
}

TFilter::~TFilter(){
    if(destination_)
        cvReleaseImage(&destination_);
}

IplImage* TFilter::processFrame(IplImage* frame){
    if(frame){
        source_=frame;
        this->apply();

        return destination_;
    }
    return NULL;//co tutaj?
}

IplImage* TFilter::getDestination(){
    return destination_;
}