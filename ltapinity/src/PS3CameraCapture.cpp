#include <cv.h>
#include <highgui.h>

#include "CLEyeMulticam.h"
#include "PS3CameraCapture.h"

#include "Constants.h"

namespace tapinity {

    PS3CameraCapture::PS3CameraCapture(int id){ 
        id_ = id;
        std::string name = PS3_CAMERA_NAME;
        new(this) PS3CameraCapture(CLEYE_COLOR_PROCESSED, CLEYE_VGA, DEFAULT_FPS, id, name); 
    }

    PS3CameraCapture::PS3CameraCapture(CLEyeCameraColorMode mode, 
                                        CLEyeCameraResolution resolution,
                                        int fps, int id, std::string name) :
                                        mode_(mode), resolution_(resolution), camera_(NULL), fps_(fps){
        int width, height;

        capture_buffer_ = NULL;
        camera_ = NULL;

        id_ = id;
        name_ = name;

        if(CLEyeGetCameraCount() < 1){
            frame_ = cvCreateImage(cvSize(DEFAULT_CAPTURE_WIDTH, DEFAULT_CAPTURE_HEIGHT), IPL_DEPTH_8U, 4);
            cvSet(frame_, cvScalar(0,0,0,0));
            is_running_ = false;
            return;
        }

        //only one PS3 camera connected assumption
        GUID guid = CLEyeGetCameraUUID(0);
        camera_ = CLEyeCreateCamera(guid, mode, resolution, float(fps));

        if(!camera_) return;

        CLEyeCameraGetFrameDimensions(camera_, width, height);

        if(mode == CLEYE_COLOR_PROCESSED || mode == CLEYE_COLOR_RAW)
            frame_ = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 4);
        else
            frame_ = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);

        is_running_ = CLEyeCameraStart(camera_);

        CLEyeSetCameraParameter(camera_, CLEYE_AUTO_GAIN, true);
        CLEyeSetCameraParameter(camera_, CLEYE_AUTO_EXPOSURE, true);
        CLEyeSetCameraParameter(camera_, CLEYE_AUTO_WHITEBALANCE, true);
    }

    PS3CameraCapture::~PS3CameraCapture(){
        cvReleaseImage(&frame_);

        if(is_running_){
            CLEyeCameraStop(camera_);
            CLEyeDestroyCamera(camera_);
            camera_ = NULL;
        }
    }

    IplImage* PS3CameraCapture::getFrame(){
        if(is_running_){
            cvGetImageRawData(frame_, &capture_buffer_);
            is_running_ = CLEyeCameraGetFrame(camera_, capture_buffer_, DEFAULT_TIMEOUT);
        }
        return frame_;
    }

    bool PS3CameraCapture::isCamera(){
        return camera_ ? true : false;
    }

    PS3Settings PS3CameraCapture::getSettings(){ 
        return settings_; 
    }

    void PS3CameraCapture::setSettings(PS3Settings settings){
        settings_ = settings;
        CLEyeSetCameraParameter(camera_, CLEYE_AUTO_GAIN, settings_.auto_gain);
        CLEyeSetCameraParameter(camera_, CLEYE_AUTO_EXPOSURE, settings_.auto_exposure);
        CLEyeSetCameraParameter(camera_, CLEYE_GAIN, settings_.gain);
        CLEyeSetCameraParameter(camera_, CLEYE_EXPOSURE, settings_.exposure);
    }
}