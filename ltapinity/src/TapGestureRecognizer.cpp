#include "TapGestureRecognizer.h"
#include "Constants.h"

namespace tapinity {
    namespace gestures {
        void TapGestureRecognizer::detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_){

            if( e->type_ != TOUCHUP || 
                gesture_->getFingers() != 1 || 
                gesture_->type_ != NONE )
                return;

            bool double_tap = false;
            float x = gesture_->getTData(0).x;
            float y = gesture_->getTData(0).y;    
            if( prev_gesture_ &&
                prev_gesture_->type_ == TAP &&
                gesture_->time_stamp_start_ - prev_gesture_->time_stamp_end_ < DOUBLETAP_TIMEOUT ){
                    float dx = abs(x - prev_gesture_->getTData(0).x);
                    float dy = abs(y - prev_gesture_->getTData(0).y);
                    double_tap = (std::max(dx,dy) < DOUBLETAP_DISTANCE);
            }

            gesture_->data_.x = x;
            gesture_->data_.y = y;

            if(double_tap){
                gesture_->type_ = DOUBLETAP;
                notifyDoubleTap(gesture_);
            } else {
                gesture_->type_ = TAP;
                notifyTap(gesture_);
            }
        }

        void TapGestureRecognizer::notifyDoubleTap(TGesture* gesture){
            for (unsigned int i = 0; i < listeners_.size(); ++i) {
                ((TapGestureListener*)listeners_[i])->doubletap(gesture);
            }
        }

        void TapGestureRecognizer::notifyTap(TGesture* gesture){
            for (unsigned int i = 0; i < listeners_.size(); ++i) {
                ((TapGestureListener*)listeners_[i])->tap(gesture);
            }
        }
    }
}