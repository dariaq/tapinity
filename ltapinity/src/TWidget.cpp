#include "TWidget.h"

namespace tapinity {
    namespace ui {

        TWidget::TWidget(float xtranslate, float ytranslate, double rotate, double scale, float width, float height, TWidget* parent) : 
                        xtranslate_(xtranslate), ytranslate_(ytranslate), rotate_(rotate), scale_(scale), width_(width), height_(height){
            initWidget(parent);
        }

        TWidget::TWidget(TWidget* parent){
            initWidget(parent);
        }

        TWidget::~TWidget(){
            delete grm_;
            for(auto it = children_.begin(); it != children_.end(); ++it){
                TWidget* temp = it->second;
                delete temp;
            }
            children_.clear();
        }

        void TWidget::initWidget(TWidget* parent){
            parent_ = parent;
            grm_ = new gestures::GestureManager();

            if(parent_)
                zindex_ = parent_->children_.size();
            else
                zindex_ = 0;
        }
    
        bool TWidget::containsPoint(TData data){
            float p1x = data.x - xtranslate_;
            float p1y = data.y - ytranslate_;

            double a = -rotate_*RAD_TO_DEG;
            double ca = cos(a);
            double sa = sin(a);
            double p2x = p1x*ca - p1y*sa;
            double p2y = p1x*sa + p1y*ca;

            double factor = scale_*0.5f;

            bool res = p2x >= - width_*factor && p2x <= width_*factor && 
                       p2y >= - height_*factor && p2y <= height_*factor;
            return res;
        }

        TWidget* TWidget::hitTest(TData data){
            if(containsPoint(data)){
                std::map<int, TWidget*>::iterator it;
                for (it = children_.begin(); it != children_.end(); ++it){
                    TWidget* ret = it->second->hitTest(data);
                    if(ret)
                        return ret;
                }
                return this;
            }
            return NULL;
        }

        void TWidget::attachWidget(TWidget* widget) {
            children_.insert(std::pair<int, TWidget*>(children_.size(), widget));
        }

        void TWidget::detachWidget(TWidget* widget){
            children_.erase(widget->zindex_);
        }

        void TWidget::addGestureRecognizer(TGestureType type){
            grm_->addGestureRecognizer(type, createGestureRecognizer(type));  
        }   

        GestureRecognizer* TWidget::createGestureRecognizer(TGestureType type){
            GestureRecognizer* gr = NULL;
            if(type == DRAG){
                gr = new DragGestureRecognizer();
                gr->addGestureListener((DragGestureListener*)this); 
            } else if(type == PINCH){
                gr = new PinchGestureRecognizer();
                gr->addGestureListener((PinchGestureListener*)this); 
            } else if(type == ROTATE){
                gr = new RotateGestureRecognizer();
                gr->addGestureListener((RotateGestureListener*)this); 
            } else if(type == TAP){
                gr = new TapGestureRecognizer();
                gr->addGestureListener((TapGestureListener*)this); 
            }
            return gr;
        }   

        void TWidget::eraseChild(int idx){ 
            children_.erase(idx); 
        }

        void TWidget::shiftLeftChild(int idx){ 
            children_[idx] = children_[idx-1]; children_[idx]->zindex_ = idx;
        }

        void TWidget::setChild(int idx, TWidget* child){ 
            children_[idx] = child; child->zindex_ = idx; 
        }

        GestureManager* TWidget::getGestureManager(){
            return grm_;
        }
    }
}
