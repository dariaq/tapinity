#include <cv.h>
#include <vector>
#include <float.h>
#include "BlobTracker.h"

namespace tapinity{
    namespace blobs{

        BlobTracker::BlobTracker(){
            finder_ = new ContoursFinder();
            currentID_ = 0;
        }

        BlobTracker::~BlobTracker(){
            delete finder_;
            finder_ = NULL;
        }

        void BlobTracker::applyTracker(IplImage* frame){
            finder_->find(frame);

            unordered_map<int,Finger>::iterator it;
            vector<Blob> blobs=finder_->getBlobs();
            unsigned int foundNo=blobs.size();
            ancestors_used_.clear();

            for(it=trackedFingers_.begin();it!=trackedFingers_.end();it++)
                it->second.active_=false;

            for(unsigned int i=0; i<foundNo; i++){
                int ancestor=FindInPreviousFrame(blobs[i]);
                if(ancestor>0 && !(std::find(ancestors_used_.begin(), ancestors_used_.end(), ancestor) != ancestors_used_.end())){
                    //kontynuacja przesuwania palca
                    blobs[i].color_= trackedFingers_[ancestor].color_;
                    trackedFingers_[ancestor].addPosition(blobs[i]);
                    trackedFingers_[ancestor].active_=true;
                    notifyTouchMove(new TEvent(TOUCHMOVE,trackedFingers_[ancestor].getTouchData()));
                    ancestors_used_.push_back(ancestor);
                }
                else{
                    //nowe dotkni�cie na ekranie
                    int id=getID();
                    Finger f=Finger(blobs[i],HISTORY,CV_RGB(rand()&255,rand()&255,rand()&255),true,id);
                    trackedFingers_[id]=f;
                    notifyTouchDown(new TEvent(TOUCHDOWN,f.getTouchData()));
                }
            }

            it=trackedFingers_.begin();
            while(it!=trackedFingers_.end()){
                if(it->second.active_==false){
                    //odj�cie palca od ekranu
                    notifyTouchUp(new TEvent(TOUCHUP,it->second.getTouchData()));
                    unusedIDs_.push_back(it->first);
                    it=trackedFingers_.erase(it);
                }
                else
                    it++;
            }

            previousFrame_.clear();
            for(it=trackedFingers_.begin();it!=trackedFingers_.end();it++)
                previousFrame_[it->first]=it->second.getLastBlob();
        }

        int BlobTracker::getID(){
            if(unusedIDs_.size()<=0)
                return ++currentID_;
            else{
                int id=unusedIDs_[unusedIDs_.size()-1];
                unusedIDs_.pop_back();
                return id;
            }
        }

        int BlobTracker::FindInPreviousFrame(Blob &blob, double epsilon){
            unordered_map<int,Blob>::iterator it;
            CvPoint2D64d distance=CvPoint2D64d();
            float min=FLT_MAX; 
            int closestBlobIndex=-1;

            for(it=previousFrame_.begin();it!=previousFrame_.end();it++){
                distance.x=blob.centroid_.x- it->second.centroid_.x;
                distance.y=blob.centroid_.y- it->second.centroid_.y;
                float length=(float)sqrt(distance.x*distance.x + distance.y*distance.y);
                if(length<epsilon && length<min){
                    closestBlobIndex=it->first;
                    min=length;
                }
            }
            return closestBlobIndex; 
        }

        vector<Finger> BlobTracker::getTrackedFingers(){
            vector<Finger> tmp;
            unordered_map<int,Finger>::iterator it;
            for(it=trackedFingers_.begin();it!=trackedFingers_.end();it++)
                tmp.push_back(it->second);
            return tmp;
        }

        void BlobTracker::attachListener(TListener* listener){
            listeners_.push_back(listener);
        }

        void BlobTracker::notifyTouchDown(TEvent* e){
            for(unsigned int i=0; i < listeners_.size(); ++i){
                listeners_[i]->touchDown(e);
            }
        }

        void BlobTracker::notifyTouchUp(TEvent* e){
            for(unsigned int i=0; i < listeners_.size(); ++i){
                listeners_[i]->touchUp(e);
            }
        }

        void BlobTracker::notifyTouchMove(TEvent* e){
            for(unsigned int i=0; i < listeners_.size(); ++i){
                listeners_[i]->touchMove(e);
            }
        }

    }
}