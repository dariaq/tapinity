#include "PinchGestureRecognizer.h"

namespace tapinity {
    namespace gestures {
        void PinchGestureRecognizer::detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_){
            if(e->type_ != TOUCHMOVE || gesture_->getFingers() != 2)
                return;

            float scale = GestureUtils::calcScale(gesture_);
            float delta = scale - gesture_->scale_;
            if( abs(1-scale) > 0.1 || 
                gesture_->type_ == PINCH ){

                    float x = gesture_->getTData(0).x;
                    float y = gesture_->getTData(0).y;
                    gesture_->scale_ = scale;
                    gesture_->type_ = PINCH;
                    gesture_->data_.x = x;
                    gesture_->data_.y = y;
                    gesture_->data_.dx = 0;
                    gesture_->data_.dy = 0;
                    gesture_->data_.scale = delta;

                    notifyPinch(gesture_);
            }
        }

        void PinchGestureRecognizer::notifyPinch(TGesture* gesture){
            for (unsigned int i = 0; i < listeners_.size(); ++i) {
                ((PinchGestureListener*)listeners_[i])->pinch(gesture);
            }
        }

    }
}