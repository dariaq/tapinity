#include "ContoursFinder.h"
using namespace tapinity::blobs;

ContoursFinder::ContoursFinder(){
    minSize_=MIN_BLOB_SIZE;
    maxSize_=MAX_BLOB_SIZE;
    max_contours_number_=MAX_CONTOURS_NR;
    max_contour_points_=MAX_CONTOUR_POINTS; 
    moments_=(CvMoments*)malloc(sizeof(CvMoments));
    input_=NULL;
}

ContoursFinder::~ContoursFinder(){
    free(moments_);
    cvReleaseImage(&input_);
}

int ContoursFinder::find(IplImage* image, int minSize, int maxSize){
    blobs_.clear();
    CvSeq* contour=NULL;
    CvMemStorage* contourSt=cvCreateMemStorage(MEM_SIZE);

    int i=0;
    if(!input_)
        input_ = cvCloneImage(image);
    else
        cvCopy(image,input_,NULL);
    cvFindContours(input_, contourSt, &contour, sizeof(CvContour), CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);
    for (;contour!=0;contour=contour->h_next)
    {
        double area=fabs(cvContourArea(contour, CV_WHOLE_SEQ));
        int countPoints = contour->total;

        if ((area< minSize_ ) || (area > maxSize_) || (countPoints>max_contour_points_))
            continue;

        blobs_.push_back(Blob());
        cvMoments(contour,moments_);

        blobs_[i].rect_=cvMinAreaRect2(contour, 0);
        if(contour->total>=6)
            blobs_[i].ellipse_=cvFitEllipse2(contour);

        CvBox2D32f box = cvMinAreaRect2(contour);
        blobs_[i].angle_ = box.angle;
        blobs_[i].area_ = area;
        blobs_[i].centroid_.x = static_cast<float>(moments_->m10/moments_->m00);
        blobs_[i].centroid_.y = static_cast<float>(moments_->m01/moments_->m00);

        i++;
    }

    if(contourSt!=NULL)
        cvReleaseMemStorage(&contourSt);

    return blobs_.size();
}

vector<Blob> ContoursFinder::getBlobs(){
    return blobs_;
}

Blob ContoursFinder::getBlob(int nr){
    return blobs_[nr];
}

void ContoursFinder::setMinSize(int minsize){
    if(minsize>0)
        minSize_=minsize;
}

void ContoursFinder::setMaxSize(int maxsize){
    if(maxsize>0)
        maxSize_=maxsize;
}

int ContoursFinder::getMinSize(){
    return minSize_;
}

int ContoursFinder::getMaxSize(){
    return maxSize_;
}