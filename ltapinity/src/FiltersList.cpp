#include "FiltersList.h"
#include "Tapinity.h"

namespace tapinity{
    namespace filters{

        FilterList::FilterList(){
            BackgroundSubtractionFilter* bsf=new BackgroundSubtractionFilter(DEFAULT_BS_NAME,NULL);
            BlurFilter* bf=new BlurFilter(DEFAULT_BLUR_NAME);
            GrayscaleFilter* gf=new GrayscaleFilter(DEFAULT_GRAY_NAME);
            HighpassFilter* hf=new HighpassFilter(DEFAULT_HIGHPASS_NAME);
            InvertFilter* inv=new InvertFilter(DEFAULT_INVERT_NAME);
            ThresholdFilter* tf=new ThresholdFilter(DEFAULT_THRESHOLD_NAME);

            inv->setActive(false);

            filters_.push_back(gf);
            filters_.push_back(inv);
            filters_.push_back(bsf);
            filters_.push_back(bf);
            filters_.push_back(hf);
            filters_.push_back(tf);
        }

        FilterList::~FilterList(){
            for(unsigned int i = 0; i < filters_.size(); ++i)
                delete filters_[i];
            filters_.clear();
        }

        IplImage* FilterList::applyFilters(IplImage* frame){
            for(unsigned int i=0; i<filters_.size();i++){
                if(filters_[i]->isActive())
                    frame=filters_[i]->processFrame(frame);
            }
            return frame;
        }

        int FilterList::size(){
            return filters_.size();
        }

        TFilter& FilterList::operator[] (unsigned int idx){
            if(idx>=0 && idx<filters_.size())
                return *filters_[idx];
            return *filters_[0];//ekhm..
        }

        TFilter* FilterList::getFilter(std::string name){
            for(unsigned int i = 0; i < filters_.size(); ++i){
                if(!filters_[i]->getName().compare(name))
                    return filters_[i];
            }
            return NULL;
        }

    }
}