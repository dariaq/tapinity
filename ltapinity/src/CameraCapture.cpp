#include <cv.h>
#include <highgui.h>

#include "CameraCapture.h"

namespace tapinity {

    CameraCapture::CameraCapture(){
        id_ = -1;
        new(this) CameraCapture(id_, DEFAULT_CAMERA_NAME); 
    }

    CameraCapture::CameraCapture(int id, std::string name){
        id_ = id;
        name_ = name;
        camera_ = cvCaptureFromCAM(id_);
        frame_ = NULL;
    }

    CameraCapture::~CameraCapture(){
        if(camera_)
            cvReleaseCapture(&camera_);
    }

    IplImage* CameraCapture::getFrame(){
        frame_ = cvQueryFrame(camera_);
        return frame_;
    }

    bool CameraCapture::isCamera(){
        return (camera_ ? true : false);
    }

}