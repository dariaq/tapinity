#include "TScreen.h"

namespace tapinity {

    TScreen::TScreen(){
        filters_ = new filters::FilterList();

        blob_tracker_ = new blobs::BlobTracker();
        blob_tracker_->attachListener((TListener*)this);

        calibration_ = new CalibrationUnit();

        file_manager_ = new FileManager();
        loadingConfig_ = false;
    }

    TScreen::~TScreen(){
        delete blob_tracker_;
        blob_tracker_ = NULL;

        delete filters_;
        filters_ = NULL;

       // delete calibration_;
        delete file_manager_;
        file_manager_ = NULL;
    }


    void TScreen::update(){
        if(!loadingConfig_){
            current_frame_ = camera_->getFrame();
            current_frame_ = filters_->applyFilters(current_frame_);

            blob_tracker_->applyTracker(current_frame_);
            processEvents();
        }
    }

    void TScreen::processEvents(){
        for(unsigned int i = 0; i < events_.size(); ++i){
            switch(events_[i].type_){
            case TOUCHDOWN:
                notifyTouchDown(&events_[i]); break;
            case TOUCHUP:
                notifyTouchUp(&events_[i]); break;
            case TOUCHMOVE:
                notifyTouchMove(&events_[i]); break;
            };
        }
        events_.clear();
    }

    void TScreen::attachListener(TListener* listener){
        listeners_.push_back(listener);
    }

    void TScreen::detachListener(TListener* listener){
        vector<TListener*>::iterator new_end = remove_if(listeners_.begin(), listeners_.end(), 
            bind2nd(equal_to<TListener*>(), listener));
        listeners_.erase(new_end, listeners_.end()); 
    }

    Camera* TScreen::loadConfig(string filename){
        loadingConfig_ = true;
        Camera* t= file_manager_->readConfig(filename, filters_, blob_tracker_->getContoursFinder(),calibration_);
        loadingConfig_ = false;
        return t;
    }

    void TScreen::saveConfig(string filename){
        file_manager_->saveConfig(filename, filters_, camera_, blob_tracker_->getContoursFinder(),calibration_);
    }

    void TScreen::touchDown(TEvent* e){
        if(calibration_->isCalibrating()){
            events_.push_back(*e);
        } else {
            TEvent calibrated;
            calibration_->cameraToScreenRect(e->data_.w, e->data_.h, e->data_.x, e->data_.y);
            calibration_->cameraToScreenPoint(e->data_.x, e->data_.y);
            e->data_.a = e->data_.w*e->data_.h;
            calibrated.data_ = e->data_;
            calibrated.type_ = TOUCHDOWN;
            calibrated.data_.dx = 0;
            calibrated.data_.dy = 0;

            events_.push_back(calibrated);
        }
    }

    void TScreen::touchUp(TEvent* e){
        if(calibration_->isCalibrating()){
            return;
        }

        TEvent calibrated;
        float tx,ty;
        tx = e->data_.x+e->data_.dx;
        ty = e->data_.y+e->data_.dy;
        calibration_->cameraToScreenRect(e->data_.w, e->data_.h, e->data_.x, e->data_.y);
        calibration_->cameraToScreenPoint(e->data_.x, e->data_.y);
        e->data_.a = e->data_.w*e->data_.h;
        calibration_->cameraToScreenPoint(tx, ty);
        calibrated.data_ = e->data_;
        calibrated.type_ = TOUCHUP;
        calibrated.data_.dx = tx-e->data_.x;
        calibrated.data_.dy = ty-e->data_.y;

        events_.push_back(calibrated);
    }

    void TScreen::touchMove(TEvent* e){
        if(calibration_->isCalibrating()){
            return;
        }

        TEvent calibrated;
        float tx,ty;
        tx = e->data_.x+e->data_.dx;
        ty = e->data_.y+e->data_.dy;
        calibration_->cameraToScreenRect(e->data_.w, e->data_.h, e->data_.x, e->data_.y);
        calibration_->cameraToScreenPoint(e->data_.x, e->data_.y);
        e->data_.a = e->data_.w*e->data_.h;
        calibration_->cameraToScreenPoint(tx, ty);
        calibrated.data_ = e->data_;
        calibrated.type_ = TOUCHMOVE;
        calibrated.data_.dx = tx-e->data_.x;
        calibrated.data_.dy = ty-e->data_.y;

        events_.push_back(calibrated);
    }

    void TScreen::notifyTouchDown(TEvent* e){
        for(unsigned int i=0; i < listeners_.size(); ++i){
            listeners_[i]->touchDown(e);
        }
    }

    void TScreen::notifyTouchUp(TEvent* e){
        for(unsigned int i=0; i < listeners_.size(); ++i){
            listeners_[i]->touchUp(e);
        }
    }

    void TScreen::notifyTouchMove(TEvent* e){
        for(unsigned int i=0; i < listeners_.size(); ++i){
            listeners_[i]->touchMove(e);
        }
    }

}