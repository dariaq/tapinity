#include <cv.h>
#include <highgui.h>

#include "HighpassFilter.h"
#include "Constants.h"

using tapinity::filters::HighpassFilter;

HighpassFilter::HighpassFilter(string name) : TFilter(name){
    level_=DEFAULT_HIGHPASS_LEVEL;
}

HighpassFilter::~HighpassFilter(){
}

void HighpassFilter::apply(){
    if(!source_)
        return;
    if(!destination_){
        destination_=cvCreateImage(cvGetSize(source_), source_->depth, source_->nChannels);
        destination_->origin = source_->origin;
    }

    int blur=level_*2+1; //nieparzysta!
    cvSmooth(source_,destination_,CV_BLUR, blur);
    cvSub(source_,destination_, destination_);
    cvSmooth(destination_,destination_,CV_BLUR,blur);
}

void HighpassFilter::setParameter(string name, int value){
    if(!name.compare(LEVEL_NAME)){
        level_ = value;
    }
}

int HighpassFilter::getParameter(string name){
    if(!name.compare(LEVEL_NAME)){
        return level_;
    }
    else return -1;
}

std::string HighpassFilter::getParameterName(){
    return LEVEL_NAME;
}