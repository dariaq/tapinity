#include <cv.h>
#include <highgui.h>

#include "GrayscaleFilter.h"

using tapinity::filters::GrayscaleFilter;

GrayscaleFilter::GrayscaleFilter(string name) : TFilter(name){
}

GrayscaleFilter::~GrayscaleFilter(){
}

void GrayscaleFilter::apply(){
    if(!source_)
        return;
    if( !destination_ )
    {
        destination_ = cvCreateImage(cvSize(source_->width,source_->height), IPL_DEPTH_8U, 1);
        destination_->origin = source_->origin;  
    }

    if(source_->nChannels != 1 && destination_->nChannels == 1) 
    {
        if(_strcmpi(source_->colorModel, "BGRA") == 0)
            cvCvtColor(source_, destination_, CV_BGRA2GRAY);
        else if(_strcmpi(source_->colorModel, "BGR") == 0)
            cvCvtColor(source_, destination_, CV_BGR2GRAY);
        else if(_strcmpi(source_->colorModel, "RGB") == 0) 
            cvCvtColor(source_, destination_, CV_RGB2GRAY);
        else if(_strcmpi(source_->colorModel, "RGBA") == 0) 
            cvCvtColor(source_, destination_, CV_RGBA2GRAY);
    }
}