#include <cv.h>
#include <highgui.h>

#include "BlurFilter.h"
#include "Constants.h"

using tapinity::filters::BlurFilter;

BlurFilter::BlurFilter(string name) : TFilter(name){
    aperture_= DEFAULT_BLUR_APERTURE;
}

BlurFilter::~BlurFilter(){
}

void BlurFilter::apply(){
    if(!source_)
        return;
    if(!destination_){
        destination_= cvCreateImage(cvGetSize(source_), source_->depth, source_->nChannels);
        destination_->origin = source_->origin;
    }
    cvSmooth(source_,destination_,kSmoothType_,aperture_);
}

void BlurFilter::setParameter(string name, int value){
    if(!name.compare(APERTURE_NAME)){
        if(value > 0){
            aperture_= value;
        }
    }
}

int BlurFilter::getParameter(string name){
    if(!name.compare(APERTURE_NAME)){
        return aperture_;
    }
    return -1;
}

std::string BlurFilter::getParameterName(){
    return APERTURE_NAME;
}