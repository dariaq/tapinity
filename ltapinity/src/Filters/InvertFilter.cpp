#include <cv.h>
#include <highgui.h>

#include "InvertFilter.h"

using tapinity::filters::InvertFilter;

InvertFilter::InvertFilter(string name) : TFilter(name) {
}

InvertFilter::~InvertFilter(){
}

void InvertFilter::apply(){
    if(!source_)
        return;
    if(!destination_){
        destination_= cvCreateImage(cvGetSize(source_), source_->depth, source_->nChannels);
        destination_->origin = source_->origin;
    }
    cvNot(source_,destination_);
}