#include <cv.h>
#include <highgui.h>

#include "ThresholdFilter.h"
#include "Constants.h"

using tapinity::filters::ThresholdFilter;

ThresholdFilter::ThresholdFilter(string name) : TFilter(name){
    threshold_=static_cast<float>(DEFAULT_THRESHOLD);
    invert_=false;
}

ThresholdFilter::~ThresholdFilter(){
}

void ThresholdFilter::apply(){
    if(!source_)
        return;
    if(!destination_){
        destination_= cvCreateImage(cvGetSize(source_), source_->depth, source_->nChannels);
        destination_->origin = source_->origin;
    }
    if(invert_) 
        cvThreshold(source_, destination_, threshold_, MAX_THRESHOLD, CV_THRESH_BINARY_INV);
    else 
        cvThreshold(source_, destination_, threshold_, MAX_THRESHOLD, CV_THRESH_BINARY);
}

void ThresholdFilter::setParameter(string name, int value){
    if(!name.compare(THRESHOLD_NAME)){
        threshold_ = static_cast<float>(value);
    }
}

int ThresholdFilter::getParameter(string name){
    if(!name.compare(THRESHOLD_NAME)){
        return static_cast<int>(threshold_);
    }
    return -1;
}

void ThresholdFilter::setInvert(bool invert){
    invert_=invert;
}

std::string ThresholdFilter::getParameterName(){
    return THRESHOLD_NAME;
}