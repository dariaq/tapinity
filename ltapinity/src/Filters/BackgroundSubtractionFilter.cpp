#include <cv.h>
#include <highgui.h>

#include "BackgroundSubtractionFilter.h"

using tapinity::filters::BackgroundSubtractionFilter;

BackgroundSubtractionFilter::BackgroundSubtractionFilter(string name, IplImage * reference) : TFilter(name){
    reference_=reference;
}

BackgroundSubtractionFilter::~BackgroundSubtractionFilter(){
    cvReleaseImage(&reference_);
}

void BackgroundSubtractionFilter::apply(){
    if(!source_)
        return;
    if(!destination_){
        destination_= cvCreateImage(cvGetSize(source_), source_->depth, source_->nChannels);
        destination_->origin = source_->origin;
    }
    if(!reference_)
        reference_=cvCloneImage(source_);

    cvSub(source_,reference_,destination_);
}

void BackgroundSubtractionFilter::setReference(IplImage* reference){
    if(reference){
        cvReleaseImage(&reference_);
        reference_=cvCloneImage(reference);
    }
}

IplImage* BackgroundSubtractionFilter::getDestination(){
    return reference_;
}
