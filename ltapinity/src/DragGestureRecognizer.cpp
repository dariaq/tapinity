#include "DragGestureRecognizer.h"

namespace tapinity {
    namespace gestures {

        void DragGestureRecognizer::detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_){
            if( e->type_ != TOUCHMOVE || gesture_->getFingers() != 1 )
                return;

            bool dist_eps = true;
            bool dist_delta = true;
            for(unsigned int i = 0; dist_eps || dist_delta, i < gesture_->init_data_.size(); ++i){
                float dx = gesture_->getTData(i).dx;
                float dy = gesture_->getTData(i).dy;
                dist_eps = sqrt(dx * dx + dy * dy) > DRAG_EPS && dist_eps;
                dist_delta = sqrt(dx * dx + dy * dy) > DRAG_EPS && dist_delta;
            }

            if(dist_delta || (dist_eps && gesture_->type_ == DRAG)){

                gesture_->type_ = DRAG;

                gesture_->data_.x = gesture_->getTData(0).x;
                gesture_->data_.y = gesture_->getTData(0).y;

                gesture_->data_.dx = gesture_->getTData(0).dx;                
                gesture_->data_.dy = gesture_->getTData(0).dy;

                notifyDrag(gesture_);
                e->cancel();
            } else if (gesture_->type_ == DRAG){
                e->cancel();
            }
        }

        void DragGestureRecognizer::notifyDrag(TGesture* gesture){
            for (unsigned int i = 0; i < listeners_.size(); ++i) {
                ((DragGestureListener*)listeners_[i])->drag(gesture);
            }
        }

    }
}