#ifndef _GESTURERECOGNIZER_H
#define _GESTURERECOGNIZER_H

#include "TGesture.h"
#include "TEvent.h"

#include "TGestureListener.h"

namespace tapinity{
    namespace gestures{

        class GestureRecognizer {
        public:
            virtual void detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_) = 0;
            void addGestureListener(TGestureListener*);

            static const float DRAG_EPS;
            static const float DRAG_DELTA;

            static const float ROTATE_EPS;
            static const float ROTATE_DELTA;

            static const float DIST_DELTA;

            static const float PINCH_EPS;
            static const float PINCH_DELTA;

        protected:
            std::vector<TGestureListener*> listeners_;
        };
    }
}

#endif // _GESTURERECOGNIZER_H

