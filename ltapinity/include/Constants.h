#ifndef _CONSTANTS_H
#define _CONSTANTS_H

#ifndef M_PI
#define M_PI        3.14159265358979323846
#endif

namespace tapinity {

    const int DEFAULT_CAPTURE_WIDTH = 640;
    const int DEFAULT_CAPTURE_HEIGHT = 480;
    const std::string PS3_CAMERA_NAME = "PS3Eye Camera";
    const std::string DEFAULT_CAMERA_NAME = "Camera";

    const int HISTORY = 10;

    const int DEFAULT_EPSILON = 50;

    const int MIN_BLOB_SIZE = 50;
    const int MAX_BLOB_SIZE = 2500;
    const int MIN_MIN_SIZE = 0;
    const int MIN_MAX_SIZE = 100;
    const int MAX_MIN_SIZE = 100;
    const int MAX_MAX_SIZE = 3000;

    const int MAX_CONTOURS_NR = 100;
    const int MAX_CONTOUR_POINTS = 200;

    const int MEM_SIZE = 1000;

    const int DEFAULT_GAIN = 70;
    const int DEFAULT_EXPOSURE = 70;

    const int DEFAULT_FPS = 60;
    const int DEFAULT_TIMEOUT = 5000;

    const int DEFAULT_BLUR_APERTURE = 5;
    const int DEFAULT_HIGHPASS_LEVEL = 10;

    const int DEFAULT_THRESHOLD = 10;
    const int MAX_THRESHOLD = 255;

    const std::string LEVEL_NAME = "level";
    const std::string APERTURE_NAME = "aperture";
    const std::string THRESHOLD_NAME = "threshold";
    const std::string DEFAULT_BS_NAME = "bsf";
    const std::string DEFAULT_BLUR_NAME = "blur";
    const std::string DEFAULT_THRESHOLD_NAME = "thresh";
    const std::string DEFAULT_HIGHPASS_NAME = "hpass";
    const std::string DEFAULT_INVERT_NAME = "inv";
    const std::string DEFAULT_GRAY_NAME = "gray";

    const int CALIBRATIONX = 4;
    const int CALIBRATIONY = 3;
    const int CALIBRATIONVERTICES = ((CALIBRATIONX+1) * (CALIBRATIONY+1));
    const int CALIBRATIONINDICES = (CALIBRATIONX * CALIBRATIONY * 6);

    const int DOUBLETAP_TIMEOUT = 300;
    const int DOUBLETAP_DISTANCE = 20;
    const int DRAG_TIMEOUT = 800;

    const double DEG_TO_RAD = 180.0f/M_PI;
    const double RAD_TO_DEG = M_PI/180.0f;

    const std::string DEFAULT_YAML_FILE = "config.yml";
}


#endif // _CONSTANTS_H
