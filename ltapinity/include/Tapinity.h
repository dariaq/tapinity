#ifndef _TAPINITY_H
#define _TAPINITY_H

#include "CLEyeMulticam.h"
#include "Camera.h"
#include "CameraCapture.h"
#include "PS3CameraCapture.h"

#include "TFilter.h"
#include "BackgroundSubtractionFilter.h"
#include "BlurFilter.h"
#include "GrayscaleFilter.h"
#include "HighpassFilter.h"
#include "InvertFilter.h"
#include "ThresholdFilter.h"
#include "FiltersList.h"

#include "Blob.h"
#include "BlobTracker.h"
#include "ContoursFinder.h"
#include "Finger.h"

#include "TScreen.h"

#include "TGesture.h"
#include "TapGestureRecognizer.h"
#include "RotateGestureRecognizer.h"
#include "PinchGestureRecognizer.h"
#include "DragGestureRecognizer.h"
#include "GestureRecognizer.h"

#include "TGestureListener.h"
#include "GestureUtils.h"

#include "TWidget.h"
#include "TWindow.h"

#include "FileManager.h"

#include "CalibrationUnit.h"

#endif // _TAPINITY_H