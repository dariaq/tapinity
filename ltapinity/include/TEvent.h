#ifndef _TEVENT_H
#define _TEVENT_H

#include "TData.h"

namespace tapinity {
    enum TEventType{
        TOUCHDOWN = 0,
        TOUCHMOVE,
        TOUCHUP
    };

    class TEvent{
    public:
        TEvent() : cancelled_(false) {}; 
        TEvent(TEventType type, TData data) : type_(type), data_(data), cancelled_(false) {};
        void cancel() { cancelled_ = true; };

        TEventType type_;
        TData data_;  
        bool cancelled_;
    };
}
#endif // _TEVENT_H
