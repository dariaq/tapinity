#ifndef _TLISTENER_H
#define _TLISTENER_H

#include "TEvent.h"

namespace tapinity {
    class TListener {
    public: 
        virtual void touchDown(TEvent* e) = 0;
        virtual void touchUp(TEvent* e) = 0;
        virtual void touchMove(TEvent* e) = 0;
    };
}

#endif // _TLISTENER_H
