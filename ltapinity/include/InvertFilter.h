#ifndef _INVERTFILTER_H
#define _INVERTFILTER_H

#include "TFilter.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace filters{

        class InvertFilter : public TFilter{

        public:
            InvertFilter(string name);
            ~InvertFilter();
            void apply();

        private:

        };

    }
}

#endif