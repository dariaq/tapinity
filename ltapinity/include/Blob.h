#ifndef _BLOB_H
#define _BLOB_H
#include <vector>
#include <cv.h>

using namespace std;

namespace tapinity{
    namespace blobs{

        class Blob{
        public:
            Blob(){
                area_=0.0;
                angle_=-1;
                centroid_.x=0;
                centroid_.y=0;
                color_= CV_RGB(0,0,0);
            }
            ~Blob(){}

            double area_;
            float angle_;
            CvPoint2D32f centroid_;
            CvScalar color_;
            CvBox2D rect_;
            CvBox2D ellipse_;

        private:

        };

    }
}
#endif