#ifndef _FILEMANAGER_H
#define _FILEMANAGER_H
#include <cv.h>
#include <vector>
#include "FiltersList.h"
#include "Camera.h"
#include "CameraCapture.h"
#include "PS3CameraCapture.h"
#include "ContoursFinder.h"
#include "CalibrationUnit.h"

namespace tapinity{
    class FileManager{
    public: 
        void saveConfig(string filename, filters::FilterList* list, Camera* camera, blobs::ContoursFinder* finder, CalibrationUnit* calibration);
        Camera* readConfig(string filename, filters::FilterList* list, blobs::ContoursFinder* finder, CalibrationUnit* calibration);
    private:
    };
}

#endif