#ifndef _HIGHPASSFILTER_H
#define _HIGHPASSFILTER_H

#include "TFilter.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace filters{

        class HighpassFilter : public TFilter{
        public:
            HighpassFilter(string name);
            ~HighpassFilter();
            void apply();

            std::string getParameterName();
            void setParameter(string name, int value);
            int getParameter(string name);

        private:
            int level_;
        };

    }
}

#endif