#ifndef _TSCREEN_H
#define _TSCREEN_H

#include <windows.h>
#include <vector>
#include <ctime>

#include <cv.h>

#include "FiltersList.h"
#include "BlobTracker.h"
#include "ContoursFinder.h"
#include "Camera.h"
#include "PS3CameraCapture.h"
#include "FileManager.h"
#include "CalibrationUnit.h"
#include "GestureRecognizer.h"


#include "TListener.h"
#include "TGestureListener.h"
#include "TEvent.h"

namespace tapinity {

  class TScreen : TListener {
  public:
    TScreen();
    ~TScreen();

    void update();

    void attachListener(TListener* listener);
    void detachListener(TListener* listener);

    virtual void touchDown(TEvent* e);
    virtual void touchUp(TEvent* e);
    virtual void touchMove(TEvent* e);

    void processEvents();

    Camera* loadConfig(string filename = DEFAULT_YAML_FILE);
    void saveConfig(string filename = DEFAULT_YAML_FILE);

    void setCamera(Camera* camera) { camera_ = camera; };
    IplImage* getFrame() { return current_frame_; };

    filters::FilterList* getFilterList() { return filters_; };
    blobs::BlobTracker* getBlobTracker() { return blob_tracker_; };
    blobs::ContoursFinder* getBlobFinder() { return blob_tracker_->getContoursFinder(); };
    CalibrationUnit* getCalibrationUnit() { return calibration_; };
    Camera* getCamera() { return camera_; }

  private:
    void notifyTouchDown(TEvent* e);
    void notifyTouchUp(TEvent* e);
    void notifyTouchMove(TEvent* e);

    bool loadingConfig_;

    filters::FilterList* filters_;
    blobs::BlobTracker* blob_tracker_;
    CalibrationUnit* calibration_;
    FileManager* file_manager_;

    Camera* camera_;
    IplImage* current_frame_;

    std::vector<TListener*> listeners_;
    std::vector<TEvent> events_;
  };

}

#endif // _TSCREEN_H