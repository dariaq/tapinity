#ifndef _ROTATEGESTURERECOGNIZER_H
#define _ROTATEGESTURERECOGNIZER_H

#include "TGesture.h"
#include "TData.h"
#include "TEvent.h"

#include "GestureRecognizer.h"

#include "GestureUtils.h"

namespace tapinity{
    namespace gestures{
        class RotateGestureRecognizer : public GestureRecognizer {
        public:
            RotateGestureRecognizer(){};
            ~RotateGestureRecognizer(){};

            void detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_);
        private:
            void notifyRotate(TGesture*);
        };
    }
}

#endif // _ROTATEGESTURERECOGNIZER_H
