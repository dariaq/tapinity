#ifndef _CAMERA_H
#define _CAMERA_H

#include <cv.h>
#include <highgui.h>

#include "Constants.h"

namespace tapinity {
    class Camera {

    public:
        virtual ~Camera(){};
        virtual IplImage* getFrame() = 0;
        virtual bool isCamera() = 0;

        bool isPS3Camera() { return !name_.compare(PS3_CAMERA_NAME); };

        int getID() { return id_; };
        std::string getName() { return name_; };

    protected:
        int id_;
        std::string name_;
    };
}

#endif // _CAMERA_H