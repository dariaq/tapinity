#ifndef _CAMERACAPTURE_H
#define _CAMERACAPTURE_H

#include "Camera.h"

namespace cv { class IplImage; }

namespace tapinity {

    class CameraCapture : public Camera {
    public:
        CameraCapture();
        CameraCapture(int idx, std::string name);
        ~CameraCapture();

        bool isCamera();

        IplImage* getFrame();

    private:
        CvCapture* camera_;
        IplImage* frame_;
    };
}

#endif // _CAMERACAPTURE_H