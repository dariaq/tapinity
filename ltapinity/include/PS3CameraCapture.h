#ifndef _PS3CAMERACAPTURE_H
#define _PS3CAMERACAPTURE_H

#include "CLEyeMulticam.h"

#include "Camera.h"

namespace cv { class IplImage; }

namespace tapinity {

    typedef struct PS3Settings {
    public:
        int gain;
        int exposure;
        bool auto_gain;
        bool auto_exposure;
        PS3Settings() : gain(DEFAULT_GAIN), exposure(DEFAULT_EXPOSURE), auto_gain(true), auto_exposure(true) {}
        PS3Settings(int g, int e, bool ag, bool ae) : gain(g), exposure(e), auto_gain(ag), auto_exposure(ae) {}
    } PS3Settings;

    class PS3CameraCapture : public Camera {
    public:
        PS3CameraCapture(int idx = 0);
        PS3CameraCapture(CLEyeCameraColorMode, CLEyeCameraResolution, int fps, int idx, std::string name);
        ~PS3CameraCapture();

        IplImage* getFrame();
        bool isCamera();

        PS3Settings getSettings();
        void setSettings(PS3Settings settings);

    private:
        IplImage* frame_;
        PBYTE capture_buffer_;
        PS3Settings settings_;

        int fps_;
        bool is_running_;

        CLEyeCameraInstance camera_;
        CLEyeCameraColorMode mode_;
        CLEyeCameraResolution resolution_;
    };
}

#endif // _PS3CAMERACAPTURE_H