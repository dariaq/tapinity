#ifndef _TGESTURELISTENER_H
#define _TGESTURELISTENER_H

#include "TGesture.h"

namespace tapinity {
    namespace gestures{
        class TGestureListener { };

        class TapGestureListener : public TGestureListener{
        public: 

            virtual void tap(TGesture* e) {};
            virtual void doubletap(TGesture* e) {};

        };

        class RotateGestureListener : public TGestureListener{
        public: 

            virtual void rotate(TGesture* e) {};

        };

        class DragGestureListener : public TGestureListener{
        public: 

            virtual void drag(TGesture* e) {};

        };

        class PinchGestureListener : public TGestureListener{
        public: 

            virtual void pinch(TGesture* e) {};

        };
    }
}

#endif // _TGESTURELISTENER_H
