#ifndef _TFILTER_H
#define _TFILTER_H

#include <string>
using namespace std;

namespace cv{
  class IplImage;
}

namespace tapinity{
  namespace filters{
    class TFilter{
      public:
        TFilter();
        TFilter(string name);
        virtual ~TFilter();
        IplImage* processFrame(IplImage* frame);
        virtual void apply()=0;
        const string getName() { return name_; }
        virtual  IplImage* getDestination();
        //showOutput, setParameter, getParameters

        bool isActive() { return active_; };
        void setActive(bool value) { active_ = value; };

        virtual std::string getParameterName() { return ""; };
        virtual void setParameter(string name, int value) {};
        virtual int getParameter(string name){ return -1; };
      protected:
        IplImage* source_;
        IplImage* destination_;
        string name_;
        std::string parameter_name_;
        bool active_;
    };
  }
}

#endif