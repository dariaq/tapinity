#ifndef _TWIDGET_H
#define _TWIDGET_H

#include <map>

#include "TData.h"
#include "TEvent.h"
#include "TGesture.h"
#include "TListener.h"

#include "GestureRecognizer.h"
#include "GestureManager.h"

#include "DragGestureRecognizer.h"
#include "TapGestureRecognizer.h"
#include "RotateGestureRecognizer.h"
#include "PinchGestureRecognizer.h"

#include "Constants.h"

using namespace tapinity::gestures;

namespace tapinity {
    namespace ui {
        class TWidget : public TListener,
                        public RotateGestureListener, 
                        public DragGestureListener, 
                        public PinchGestureListener,
                        public TapGestureListener {
        public:
            TWidget(float xtranslate, float ytranslate, double rotate, double scale, float width, float height, TWidget* parent_ = NULL);
            TWidget(TWidget* parent = NULL);
            ~TWidget();

            void initWidget(TWidget* parent);
            TWidget* hitTest(TData);

            void attachWidget(TWidget*);
            void detachWidget(TWidget*);

            virtual void onTouchDown(TEvent*){};
            virtual void onTouchUp(TEvent*){};
            virtual void onTouchMove(TEvent*){};

            virtual void touchDown(TEvent*){};
            virtual void touchUp(TEvent*){};
            virtual void touchMove(TEvent*){};

            virtual void tap(TGesture*){};
            virtual void doubletap(TGesture*){};
            virtual void rotate(TGesture*){};
            virtual void drag(TGesture*){};
            virtual void pinch(TGesture*){};

            void addGestureRecognizer(TGestureType);
            GestureRecognizer* createGestureRecognizer(TGestureType);
            
            void eraseChild(int idx);
            void shiftLeftChild(int idx);
            void setChild(int idx, TWidget* child);
            GestureManager* getGestureManager();

        protected:
            virtual bool containsPoint(TData);
            
            float xtranslate_;
            float ytranslate_;
            double rotate_;
            double scale_;

            float width_;
            float height_;

            int zindex_;

            GestureManager* grm_;

            TWidget* parent_;   
            std::map<int,TWidget*> children_;
        };
    }
}

#endif // _TWIDGET_H