#ifndef _FINGER_H
#define _FINGER_H

#include "Blob.h"
#include "TData.h"

namespace tapinity{
    namespace blobs{

        class Finger{
        public:
            Finger();
            Finger(Blob b,int age, CvScalar color, bool active, int ID);
            ~Finger();

            void addPosition(Blob b);
            Blob getLastBlob();

            TData getTouchData();

            CvScalar color_;
            unsigned int age_;
            bool active_;
            int ID_;

        private:
            CvPoint2D32f getDelta();
            vector<Blob> track_;
        };

    }
}

#endif