#ifndef _TDATA_H
#define _TDATA_H

struct TData {
    float x;
    float y;

    float h;
    float w;

    float a;

    float dx;
    float dy;
    int ID;
};

#endif // _TDATA_H
