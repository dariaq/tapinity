#ifndef _CONTOURSFINDER_H
#define _CONTOURSFINDER_H

#include <vector>
#include "Blob.h"

#include "Constants.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace blobs{

        class ContoursFinder{
        public:
            ContoursFinder();
            ~ContoursFinder();

            int find(IplImage* image, int minSize=MIN_BLOB_SIZE, int maxSize=MAX_BLOB_SIZE);

            vector<Blob> getBlobs();
            Blob getBlob(int nr);

            void setMinSize(int minsize);
            void setMaxSize(int maxsize);
            int getMinSize();
            int getMaxSize();

        private:
            IplImage* input_;
            vector<Blob> blobs_;
            CvMoments* moments_;
            int minSize_;
            int maxSize_;
            int max_contours_number_;
            int max_contour_points_;
        };
    }
}

#endif