#ifndef _BLURFILTER_H
#define _BLURFILTER_H

#include "TFilter.h"
#include "Constants.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace filters{

        class BlurFilter : public TFilter{
        public:
            BlurFilter(string name);
            ~BlurFilter();
            void apply();

            void setParameter(string name, int value);
            int getParameter(string name);
            std::string getParameterName();

        private:
            static const int kSmoothType_ = CV_BLUR;
            int aperture_;
        };

    }
}

#endif