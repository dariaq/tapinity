#ifndef _GESTUREUTILS_H
#define _GESTUREUTILS_H

#include "TGesture.h"
#include "TData.h"
#include "GestureRecognizer.h"

#include "Constants.h"

namespace tapinity{
    namespace gestures{
        class GestureUtils {
        public:
            static double calcAngle(TGesture* gesture_);
            static double calcAngle(float, float, float, float);            

            static float calcDistance(TGesture* gesture_);
            static float calcDistance(float, float, float, float);

            static float calcScale(TGesture* gesture_);
        };
    }
}

#endif // _GESTUREUTILS_H