#ifndef _TGESTURE_H
#define _TGESTURE_H

#include <vector>
#include <ctime>

#include "TData.h"

namespace tapinity{
    namespace gestures{

        enum TGestureType{
            NONE = 0,
            ROTATE,
            PINCH,
            DRAG,
            DOUBLETAP,
            TAP
        };

        struct TGestureData {
            float x;
            float y;
            float dx;
            float dy;
            float dist;
            double angle;
            double scale;
        };

        class TGesture{
        public:
            TGesture();
            TGesture(TData data);
            ~TGesture();

            int getFingers() { return fingers_.size(); }
            TData getTData(int idx) { return fingers_[idx]; }

            void pushFinger(TData data);

            void popFinger(TData data);
            void replaceFinger(TData data);

            clock_t time_stamp_start_;
            clock_t time_stamp_end_;

            double angle_;
            float scale_;         

            TGestureType type_;
            TGestureData data_;

            std::vector<TData> fingers_;
            std::vector<TGestureData> init_data_;

        };
    }
}

#endif // _TGESTURE_H