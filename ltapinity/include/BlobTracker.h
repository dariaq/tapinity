#ifndef _BLOBTRACKER_H
#define _BLOBTRACKER_H

#include <unordered_map>
#include <vector>

#include "Blob.h"
#include "Finger.h"
#include "ContoursFinder.h"

#include "TEvent.h"
#include "TListener.h"

#include "Constants.h"

namespace tapinity{
  namespace blobs{

    class BlobTracker
    {
    public:
      BlobTracker();
      ~BlobTracker();

      void applyTracker(IplImage* frame);
      std::vector<Finger> getTrackedFingers();
      ContoursFinder* getContoursFinder(){ return finder_; }
      
      void attachListener(TListener* listener);

    private:
      int FindInPreviousFrame(Blob &blob, double epsilon=DEFAULT_EPSILON);
      int getID();

      void notifyTouchDown(TEvent* e);
      void notifyTouchUp(TEvent* e);
      void notifyTouchMove(TEvent* e);

      std::unordered_map<int,Finger> trackedFingers_;
      std::unordered_map<int,Blob> previousFrame_;
      std::vector<int> unusedIDs_;
      std::vector<int> ancestors_used_;

      ContoursFinder* finder_;
      int minThreshold_;
      int currentID_;

      std::vector<TListener*> listeners_;
    };

  }
}
#endif