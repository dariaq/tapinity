#ifndef _CALIBRATIONUNIT_H
#define _CALIBRATIONUNIT_H

#include <cv.h>

#include "Constants.h"

namespace tapinity{
    class CalibrationUnit{
    public:
        CalibrationUnit();
        ~CalibrationUnit();

        void begin(IplImage* image);
        void step(float x, float y);
        void revert();

        bool isCalibrated(){
            return isCalibrated_;
        }

        void setIsCalibrated(bool value){
            isCalibrated_ = value; 
        }

        bool isCalibrating(){
            return calibrating_;
        }


        int getStep(){ return step_; }
        void cameraToScreenPoint(float &x, float &y);
        void cameraToScreenRect(float &w, float &h, float midX, float midY);

        CvPoint2D32f getScreenPoint(unsigned int n){ return screen_[n]; }
        CvPoint2D32f getCameraPoint(unsigned int n){ return camera_[n]; }
        void setCameraPoint(CvPoint2D32f point, int n) { camera_[n]=point; }
        CvPoint2D32f* getCameraPoints(){ return camera_; }

        int* getTraingles() { return triangles_; } 

        void initCameraScreen();
        void setCameraPoint(unsigned int no, float x, float y);
        void mapCameraToScreen();
    private:


        int assignToTriangle(float x, float y);
        bool isInTriangle(float x, float y, CvPoint2D32f a, CvPoint2D32f b, CvPoint2D32f c);
        float side(float x, float y, CvPoint2D32f p1, CvPoint2D32f p2);

        void calculateCameraToScreenPoint(float &x, float &y);

        IplImage* image_;

        int triangles_[CALIBRATIONINDICES];
        CvPoint2D32f camera_[CALIBRATIONVERTICES];
        CvPoint2D32f screen_[CALIBRATIONVERTICES];
        float*** cameraToScreen_;
        int width_, height_; //camera size

        CvPoint2D32f screenLowerRight, screenUpperLeft;
        CvPoint2D32f minCameraPoint, maxCameraPoint;

        unsigned int step_;

        bool calibrating_;
        bool isCalibrated_;

        bool second_click;
    };
}


#endif
