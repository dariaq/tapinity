#ifndef _FILTERSLIST_H
#define _FILTERSLIST_H

#include <vector>
#include <cv.h>

#include <highgui.h>

#include "TFilter.h"
#include "InvertFilter.h"

namespace tapinity{
    namespace filters{

        class FilterList {
        public:
            FilterList();
            ~FilterList();

            IplImage* applyFilters(IplImage* frame);
            TFilter& operator[] (unsigned int idx);
            TFilter* getFilter(std::string name);
            int size();

        private:
            vector<TFilter*>filters_;
        };

    }
}
#endif