#ifndef _DRAGGESTURERECOGNIZER_H
#define _DRAGGESTURERECOGNIZER_H

#include "TGesture.h"
#include "TData.h"
#include "TEvent.h"

#include "GestureRecognizer.h"

#include "GestureUtils.h"

namespace tapinity{
    namespace gestures{
        class DragGestureRecognizer : public GestureRecognizer {
        public:
            DragGestureRecognizer(){};
            ~DragGestureRecognizer(){};         

            void detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_);

        private:
            void notifyDrag(TGesture*);
        };
    }
}

#endif // _DRAGGESTURERECOGNIZER_H
