#ifndef _GESTUREMANAGER_H
#define _GESTUREMANAGER_H

#include <vector>

#include "TGesture.h"
#include "TData.h"
#include "TEvent.h"
#include "TListener.h"
#include "GestureRecognizer.h"

#include "GestureUtils.h"

namespace tapinity{
    namespace gestures{
        class GestureManager : TListener {
        public:
            GestureManager();
            ~GestureManager();

            void touchDown(TEvent* e);
            void touchUp(TEvent* e);
            void touchMove(TEvent* e);

            void addGestureRecognizer(TGestureType, GestureRecognizer*);

            TGesture* getGesture() { return gesture_; };
            TGesture* getPrevGesture() { return prev_gesture_; };

            void setGesture(TGesture* g) { gesture_ = g;} ;
            void setPrevGesture(TGesture* g) { prev_gesture_ = g;} ;

            void swapGestures();
        private:
            TGesture* prev_gesture_;
            TGesture* gesture_;

            std::vector<GestureRecognizer*> gesture_recognizers_;
        };
    }
}

#endif // _GESTUREMANAGER_H