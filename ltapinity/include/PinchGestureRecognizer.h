#ifndef _PINCHGESTURERECOGNIZER_H
#define _PINCHGESTURERECOGNIZER_H

#include "TGesture.h"
#include "TData.h"
#include "TEvent.h"

#include "GestureRecognizer.h"

#include "GestureUtils.h"

namespace tapinity{
    namespace gestures{
        class PinchGestureRecognizer : public GestureRecognizer {
        public:
            PinchGestureRecognizer(){};
            ~PinchGestureRecognizer(){};

            void detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_);
        private:
            void notifyPinch(TGesture*);
        };
    }
}

#endif // _PINCHGESTURERECOGNIZER_H
