#ifndef _TWINDOW_H
#define _TWINDOW_H

#include <map>

#include "TWidget.h"
#include "TListener.h"
#include "TData.h"

namespace tapinity {
    namespace ui {
        class TWindow : public TWidget {
        public:
            TWindow();
            ~TWindow();

            void touchDown(TEvent* e);
            void touchUp(TEvent* e);
            void touchMove(TEvent* e);

            bool containsPoint(TData data) { return true; };

        protected:          
            std::map<int,TWidget*> events_;
        };
    }
}

#endif // _TWINDOW_H
