#ifndef _THRESHOLDFILTER_H
#define _THRESHOLDFILTER_H

#include "TFilter.h"

namespace cv{
        class IplImage;
    }

namespace tapinity{
namespace filters{

class ThresholdFilter : public TFilter{
public:
    ThresholdFilter(string name);
    ~ThresholdFilter();
    void apply();

    void setParameter(string name, int value);
    int getParameter(string name);
    std::string getParameterName();

    void setInvert(bool invert);

private:
    float threshold_;
    bool invert_;
};

}
}

#endif