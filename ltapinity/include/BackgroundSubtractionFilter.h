#ifndef _BACKGROUNDSUBTRACTIONFILTER_H
#define _BACKGROUNDSUBTRACTIONFILTER_H

#include "TFilter.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace filters{

        class BackgroundSubtractionFilter : public TFilter{
        public:
            BackgroundSubtractionFilter(string name, IplImage * reference);
            ~BackgroundSubtractionFilter();
            void apply();

            void setReference(IplImage* reference);
            IplImage* getDestination();

        private:
            IplImage* reference_;
        };

    }
}

#endif