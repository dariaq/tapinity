#ifndef _TAPGESTURERECOGNIZER_H
#define _TAPGESTURERECOGNIZER_H

#include "TGesture.h"
#include "TData.h"
#include "TEvent.h"

#include "GestureRecognizer.h"

#include "Constants.h"

namespace tapinity{
    namespace gestures{
        class TapGestureRecognizer : public GestureRecognizer {
        public:
            TapGestureRecognizer(){};
            ~TapGestureRecognizer(){};

            void detectGesture(TEvent* e, TGesture* gesture_, TGesture* prev_gesture_);
        private:
            void notifyTap(TGesture*);
            void notifyDoubleTap(TGesture*);
        };
    }
}

#endif // _TAPGESTURERECOGNIZER_H
