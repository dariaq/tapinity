#ifndef _GRAYSCALEFILTER_H
#define _GRAYSCALEFILTER_H

#include "TFilter.h"

namespace cv{
    class IplImage;
}

namespace tapinity{
    namespace filters{

        class GrayscaleFilter : public TFilter{

        public:
            GrayscaleFilter(string name);
            ~GrayscaleFilter();
            void apply();

        private:

        };

    }
}

#endif