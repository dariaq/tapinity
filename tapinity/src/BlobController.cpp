#include "BlobController.h"

namespace tb = ::tapinity::blobs;

BlobController::BlobController(tb::BlobTracker* bt){
    tracker_ = bt;
    finder_= tracker_->getContoursFinder();
}

BlobController::~BlobController(){
    tracker_ = NULL;
    finder_ = NULL;
}

void BlobController::frameAction(IplImage* calculations_image, IplImage* draw_image, bool boundingBoxes){
    vector<tb::Finger> trackedFingers=tracker_->getTrackedFingers();
    unsigned int size=trackedFingers.size();
    for(unsigned int i=0; i<size; i++){
        drawBlob(draw_image,  trackedFingers[i].getLastBlob(),i,true);
        //drawBlobMovement(draw_image,  trackedFingers[i].getOrigin(),  trackedFingers[i].getLastBlob());
    }
}

void BlobController::drawBlob(IplImage* image,tb::Blob b,int n, bool boundingBox){

    CvPoint2D32f boxPoints[4];
    cvBoxPoints(b.rect_, boxPoints);
    cvLine(image,cvPoint((int)boxPoints[0].x, (int)boxPoints[0].y),cvPoint((int)boxPoints[1].x, (int)boxPoints[1].y),b.color_,1,CV_AA,0);
    cvLine(image,cvPoint((int)boxPoints[1].x, (int)boxPoints[1].y),cvPoint((int)boxPoints[2].x, (int)boxPoints[2].y),b.color_,1,CV_AA,0);
    cvLine(image,cvPoint((int)boxPoints[2].x, (int)boxPoints[2].y),cvPoint((int)boxPoints[3].x, (int)boxPoints[3].y),b.color_,1,CV_AA,0);
    cvLine(image,cvPoint((int)boxPoints[3].x, (int)boxPoints[3].y),cvPoint((int)boxPoints[0].x, (int)boxPoints[0].y),b.color_,1,CV_AA,0);

    if(b.ellipse_.size.width>0.0)
        cvEllipseBox(image, b.ellipse_,b.color_, 3, CV_AA, 0); 
    
    char text[255]; 
    sprintf(text, "ID %d", (int)n);

    CvFont font;
    cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, 0.8, 0.8, 0, 1, CV_AA);
    image->origin=IPL_ORIGIN_BL;
    cvPutText(image, text, cvPoint(b.centroid_.x, b.centroid_.y), &font, cvScalar(255, 255, 255, 0));
    image->origin=IPL_ORIGIN_TL;
}

void BlobController::drawBlobMovement(IplImage* image, tb::Blob origin, tb::Blob current){
    cvLine(image, cvPointFrom32f(origin.centroid_), cvPointFrom32f(current.centroid_), current.color_, 2,CV_AA);
}

void BlobController::setMinBlobSize(int minsize){
    finder_->setMinSize(minsize);
    //cout<<"min blob size "<<minsize<<endl;
}

void BlobController::setMaxBlobSize(int maxsize){
    finder_->setMaxSize(maxsize);
    //cout<<"max blob size "<<maxsize<<endl;
}

int BlobController::getMinBlobSize(){
    return finder_->getMinSize();
}

int BlobController::getMaxBlobSize(){
    return finder_->getMaxSize();
}