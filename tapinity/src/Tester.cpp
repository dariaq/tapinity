#include <iostream>
#include <QTimer>
#include <QMessageBox>

#include "Tester.h"

#include <cv.h>
#include <highgui.h>
#include "Tapinity.h"

using namespace tapinity;

TesterWidget::TesterWidget(TScreen* screen, QWidget *parent) : QGLWidget(parent){
    screen_=screen;
    timer_=new QTimer(this);
    connect(timer_, SIGNAL(timeout()), this, SLOT(updateGL()));
}

TesterWidget::~TesterWidget(){
    delete timer_;
}

void TesterWidget::initializeGL(){
    glViewport(0,0,this->width(), this->height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-this->width()/2, this->width()/2, this->height()/2, -this->height()/2, -1, 1); 

    glClearColor(0.529f, 0.808f, 0.980f, 1.0f);
    glClearDepth(1.0);

    glDepthFunc(GL_LESS);
    glDisable(GL_DEPTH_TEST);
}

void TesterWidget::initialize(){
    timer_->start(1000/45);//FPS
    this->showFullScreen();
}

void TesterWidget::paintGL()
{   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    int* triangles = screen_->getCalibrationUnit()->getTraingles();
    for(int i=0; i<CALIBRATIONINDICES; i+=3){
        drawTriangle(screen_->getCalibrationUnit()->getScreenPoint(triangles[i]),screen_->getCalibrationUnit()->getScreenPoint(triangles[i+1]),screen_->getCalibrationUnit()->getScreenPoint(triangles[i+2]));
    }
    for(int i=0; i<to_draw.size();i++){
        drawFinger(to_draw[i].x*2.0f-1.0f, (1.0f-to_draw[i].y)*2.0f-1.0f,0.05,1.0,0.0,0.0,0.0);
    }
    to_draw.clear();
}

void TesterWidget::drawTriangle(CvPoint2D32f p0, CvPoint2D32f p1, CvPoint2D32f p2)
{
    glPushMatrix();
    glLoadIdentity();
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glColor3f(0.0,0.0,1.0);
    glBegin(GL_LINES);
    glVertex2f(p0.x*2.0f-1.0f, p0.y*2.0f-1.0f); 
    glVertex2f(p1.x*2.0f-1.0f, p1.y*2.0f-1.0f);
    glVertex2f(p1.x*2.0f-1.0f, p1.y*2.0f-1.0f); 
    glVertex2f(p2.x*2.0f-1.0f, p2.y*2.0f-1.0f); 
    glVertex2f(p2.x*2.0f-1.0f, p2.y*2.0f-1.0f); 
    glVertex2f(p0.x*2.0f-1.0f, p0.y*2.0f-1.0f); 
    glEnd();
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    glPopMatrix();
}

void TesterWidget::touchDown(TEvent* e){
    to_draw.push_back(e->data_);
}

void TesterWidget::touchUp(TEvent* e){
    to_draw.push_back(e->data_);
}

void TesterWidget::touchMove(TEvent* e){
    to_draw.push_back(e->data_);
}

void TesterWidget::drawFinger(float x1, float y1, float s, float r, float g, float b, float ang)
{

    glPushMatrix();
    glLoadIdentity();
    glTranslatef(x1, y1, 0.0);
    glScalef(s, s, s);

    int i;
    int sections = 20; 
    GLfloat radius = 0.8f; 
    GLfloat twoPi = 2.0f * 3.14159f;

    glColor3f(r,g,b); 
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0.0, 0.0); 
    for(i = 0; i <= sections;i++) { 
        glVertex2f(radius * cos(i *  twoPi / sections), 
            radius* sin(i * twoPi / sections));
    }
    glEnd();

    glPopMatrix();
}

void TesterWidget::debugInfo(){
    for(unsigned int i=0; i<to_draw.size();i++){
        cout<<i<<" to draw "<<to_draw[i].x<<" "<<to_draw[i].y<<endl;
    }
    CalibrationUnit* calib = screen_->getCalibrationUnit();
    cout<<"SCREEN POINTS"<<endl;
    for(int i=0; i<CALIBRATIONVERTICES; i++){
        CvPoint2D32f p = calib->getScreenPoint(i);
        cout<<i<<" : "<<p.x<<" "<<p.y<<endl;
    }
    cout<<"CAMERA POINTS"<<endl;
    for(int i=0; i<CALIBRATIONVERTICES; i++){
        CvPoint2D32f p =calib->getCameraPoint(i);
        cout<<i<<" : "<<p.x<<" "<<p.y<<endl;
    }
}

void TesterWidget::keyPressEvent(QKeyEvent *k)
{
    switch (k->key()) {
    case Qt::Key_Q:
        screen_->detachListener((TListener*)this);
        close();
        break;
    case Qt::Key_D:
        debugInfo();
        break;
    }

}