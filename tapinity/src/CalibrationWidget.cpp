#include <iostream>
#include <QTimer>
#include <QMessageBox>

#include <cv.h>
#include <highgui.h>

#include "CalibrationWidget.h"

CalibrationWidget::~CalibrationWidget(){}

void CalibrationWidget::initializeGL(){
    glViewport(0,0,this->width(), this->height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-this->width()/2, this->width()/2, this->height()/2, -this->height()/2, -1, 1); 

    glEnable( GL_POINT_SMOOTH );
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glLineWidth(2.0);
}

void CalibrationWidget::initialize(IplImage* image){
    calibration_->begin(image);
    showFullScreen();
}

void CalibrationWidget::paintGL()
{   
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glColor3f(0.0f, 0.0f, 0.0f);

    for(int i=0; i<CALIBRATIONVERTICES;i++){
        CvPoint2D32f p =calibration_->getScreenPoint(i); 
        if(calibration_->getStep()==i) 
            drawFinger((p.x*2.0f)-1.0f, ((1.0-p.y)*2.0f)-1.0f, 0.05, 0.258824 ,0.258824 ,0.435294, 0.0);
        else
            drawFinger((p.x*2.0f)-1.0f, ((1.0-p.y)*2.0f)-1.0f, 0.05,0.196078,0.8,0.196078, 0.0);
    }

}

void CalibrationWidget::keyPressEvent(QKeyEvent *k)
{
    switch (k->key()) {
    case Qt::Key_R:
        if(calibration_->isCalibrating()){
            calibration_->revert();
            updateGL();
        }
        break;
    }
}

void CalibrationWidget::drawFinger(float x1, float y1, float s, float r, float g, float b, float ang)
{

    glPushMatrix();
    glLoadIdentity();
    glTranslatef(x1, y1, 0.0);
    glScalef(s, s, s);

    int i;
    int sections = 20; 
    GLfloat radius = 0.8f; 
    GLfloat twoPi = 2.0f * 3.14159f;

    glColor3f(r,g,b); 
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(0.0, 0.0); 
    for(i = 0; i <= sections;i++) { 
        glVertex2f(radius * cos(i *  twoPi / sections), 
            radius* sin(i * twoPi / sections));
    }
    glEnd();

    glPopMatrix();
}

void CalibrationWidget::touchDown(TEvent* e){
    cout<<"calibration point:"<<e->data_.x<<" "<<e->data_.y<<endl;
    calibration_->step(e->data_.x, e->data_.y);
    updateGL();
}

void CalibrationWidget::touchUp(TEvent* e){
    if(calibration_->isCalibrated()){
        screen_->detachListener((TListener*)this);
        close();
    }
}

void CalibrationWidget::touchMove(TEvent* e){
}
