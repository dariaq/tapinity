#include <iostream>
#include <QTimer>
#include <QMessageBox>

#include "GLWidget.h"

#include <cv.h>
#include <highgui.h>
#include "Tapinity.h"

using namespace tapinity;

GLWidget::GLWidget(QWidget *parent) : QGLWidget(parent), m_image(0), filter_(NULL), camera_(NULL) {}

GLWidget::GLWidget(filters::TFilter* filter, QWidget *parent) : QGLWidget(parent), m_image(0), filter_(filter), camera_(NULL) {}

GLWidget::GLWidget(Camera* camera, QWidget *parent) : QGLWidget(parent), m_image(0), filter_(NULL), camera_(camera) {}

GLWidget::~GLWidget(){}

void GLWidget::initializeGL(){
    glViewport(0,0,this->width(), this->height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, this->width(), this->height(),0, -1, 1);     

    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}


void GLWidget::paintGL(){   
    glClear (GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glScalef(1.0f, -1.0f, 1.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if(m_image){
        int image_data_type = GL_LUMINANCE;
        if(m_image->nChannels == 3)
            image_data_type = GL_RGB;
        else if(m_image->nChannels == 4)
            image_data_type = GL_RGBA;
        int s = this->height();
        glBindTexture(GL_TEXTURE_2D, texture_);
        glTexImage2D(GL_TEXTURE_2D, 0, image_data_type, m_image->width, m_image->height, 0, image_data_type, GL_UNSIGNED_BYTE, m_image->imageData);
        glBegin(GL_QUADS);
            glTexCoord2i(0,0); glVertex2i(0,this->height());
            glTexCoord2i(0,1); glVertex2i(0,0);
            glTexCoord2i(1,1); glVertex2i(this->width(),0);
            glTexCoord2i(1,0); glVertex2i(this->width(),this->height());
        glEnd();
    }
}

void GLWidget::setFrame(IplImage* img){
    m_image = img;
    glDraw(); 
}

void GLWidget::setFrame(){
    if(filter_)
        m_image = filter_->getDestination();
    else if(camera_)
        m_image = camera_->getFrame();
    glDraw(); 
}

void GLWidget::mousePressEvent(QMouseEvent*){
    emit clicked();
}