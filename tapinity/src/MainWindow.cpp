#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow){
        bool is_camera = true;
        ui->setupUi(this);
        
        fps_ = 1000.0f/DEFAULT_FPS;

        timer_filters_ = new QTimer();
        timer_camera_ = new QTimer();

        timer_filters_->setInterval(fps_);
        timer_camera_->setInterval(fps_);

        initCamerasTab();

        if(cameras_.size() < 1){
            QMessageBox::critical(this, "Error", "No camera found.");
            capture_ = new CameraCapture();
            cameras_.push_back(capture_);
            is_camera = false;
        } else {
            capture_ = cameras_[0];
        }

        screen_ = new TScreen();
        screen_->setCamera(capture_);

        output_image_ = capture_->getFrame();
        list_ = screen_->getFilterList();
        controller_ = new BlobController(screen_->getBlobTracker());

        initGLWidgets();

        //wait for camera initialization to substract background
        Sleep(2000);

        initFiltersFrames();

        initFiltersSliders();
        initBlobSliders();


        calibration_ = new CalibrationWidget(screen_, screen_->getCalibrationUnit()); 
        tester_ = new TesterWidget(screen_);

        //UI components initialization
        connect(ui->bsf_button, SIGNAL(clicked()), this, SLOT(substractBackground()));
        connect(ui->invert_checkbox, SIGNAL(stateChanged(int)), this, SLOT(invertImage()));
        connect(ui->tab_widget, SIGNAL(currentChanged(int)), this, SLOT(changeTimers(int)));
        connect(ui->camera_config_btn, SIGNAL(clicked()), this, SLOT(openConfigDialog()));

        connect(ui->save_config_menu, SIGNAL(triggered()), this, SLOT(saveConfig()));
        connect(ui->load_config_menu, SIGNAL(triggered()), this, SLOT(loadConfig()));
        connect(ui->calibrate_menu, SIGNAL(triggered()), this, SLOT(calibrate()));
        connect(ui->test_menu, SIGNAL(triggered()), this, SLOT(testCalibration()));

        if(is_camera)
            timer_filters_->start(); 
}

MainWindow::~MainWindow(){
    delete calibration_;
    delete tester_;    

    for(unsigned int i = 0; i < cameras_.size(); ++i)
        delete cameras_[i];

    delete screen_;
    delete controller_;

    delete timer_filters_;
    delete timer_camera_;

    delete ui;
}

void MainWindow::setTimers(QTimer* timer1, QTimer* timer2){
    timer1->stop();
    timer2->start();
}

void MainWindow::changeTimers(int idx){
    if(idx == 0){
        setTimers(timer_camera_, timer_filters_);
    } else {
        setTimers(timer_filters_, timer_camera_);
    }
}

void MainWindow::getOutput(){
    screen_->update();
    calculations_image_ = screen_->getFrame();
    output_gl_widget->setFrame(calculations_image_);
    controller_->frameAction(calculations_image_, output_image_);
    camera_gl_widget->setFrame(output_image_);
}

void MainWindow::initFiltersFrames(){
    tf::FilterList& temp = *list_;

    for(unsigned int i = 0; i < temp.size(); ++i){
        GLWidget* filter = new GLWidget(&(temp[i]));
        filters_widgets_.push_back(filter);

        filter->setFixedHeight(DEFAULT_HEIGHT);
        filter->setFixedWidth(DEFAULT_WIDTH);

        std::string name = temp[i].getName();
        QTabWidget* tab = new QTabWidget();
        ui->tabWidget->addTab(tab, QString::fromStdString(name));
        QVBoxLayout* box = new QVBoxLayout(tab);
        box->addWidget(filter);

        connect(timer_filters_, SIGNAL(timeout()), filter, SLOT(setFrame()));
    }
}

void MainWindow::initFiltersSliders(){
    tf::FilterList& temp = *list_;

    std::string param, name;
    for(unsigned int i = 0; i < temp.size(); ++i){
        param = temp[i].getParameterName();

        if(!param.length()) continue;

        name = temp[i].getName();

        QWidget* widget = new QWidget(ui->sliders_layout->parentWidget());
        widget->setMinimumHeight(37);
        QHBoxLayout* box = new QHBoxLayout;

        ComponentSlider* slider = new ComponentSlider(Qt::Horizontal, &temp[i], param);
        QString slider_label = QString::fromStdString("slider_" + param);
        slider->setObjectName(slider_label);

        if(!param.compare(THRESHOLD_NAME))
            slider->setMaximum(MAX_THRESHOLD);

        FilterCheckbox* checkbox = new FilterCheckbox(&temp[i], name);
        QString checkbox_label = QString::fromStdString("checkbox_" + param);
        checkbox->setObjectName(checkbox_label);

        box->addWidget(new QLabel(QString::fromStdString(name)));
        box->addWidget(slider);
        box->addWidget(checkbox);

        widget->setLayout(box);
        ui->sliders_layout->addWidget(widget);

        connect(slider, SIGNAL(valueChanged(int)), slider, SLOT(setFilter(int)));
        connect(checkbox, SIGNAL(stateChanged(int)), checkbox, SLOT(setActive()));

        connect(this, SIGNAL(updateSlider(std::string, int)), slider, SLOT(setSlider(std::string, int)));
        connect(this, SIGNAL(updateCheckbox(std::string, bool)), checkbox, SLOT(setActive(std::string, bool)));
    }
}

void MainWindow::initBlobSliders(){
    ComponentSlider* min_blob_slider = new ComponentSlider(Qt::Horizontal, "min_blob");
    ComponentSlider* max_blob_slider = new ComponentSlider(Qt::Horizontal, "max_blob");

    min_blob_slider->setObjectName("min_blob");
    max_blob_slider->setObjectName("max_blob");

    min_blob_slider->setMinimum(MIN_MIN_SIZE);
    min_blob_slider->setMaximum(MIN_MAX_SIZE);
    max_blob_slider->setMinimum(MAX_MIN_SIZE);
    max_blob_slider->setMaximum(MAX_MAX_SIZE);

    min_blob_slider->setValue(controller_->getMinBlobSize());
    max_blob_slider->setValue(controller_->getMaxBlobSize());

    QWidget* widget = new QWidget;
    widget->setMinimumHeight(37);
    QHBoxLayout* box = new QHBoxLayout;

    box->addWidget(new QLabel(QString::fromStdString("Minimum blob size")));
    box->addWidget(min_blob_slider);

    box->addWidget(new QLabel(QString::fromStdString("Maximum blob size")));
    box->addWidget(max_blob_slider);

    widget->setLayout(box);
    ui->sliders_layout->addWidget(widget);

    connect(min_blob_slider, SIGNAL(valueChanged(int)), controller_, SLOT(setMinBlobSize(int)));
    connect(max_blob_slider, SIGNAL(valueChanged(int)), controller_, SLOT(setMaxBlobSize(int)));

    connect(this, SIGNAL(updateSlider(std::string, int)), min_blob_slider, SLOT(setSlider(std::string, int)));
    connect(this, SIGNAL(updateSlider(std::string, int)), max_blob_slider, SLOT(setSlider(std::string, int)));
}

void MainWindow::substractBackground(){
    filters::BackgroundSubtractionFilter* bsf = (filters::BackgroundSubtractionFilter*)list_->getFilter(DEFAULT_BS_NAME);
    filters::InvertFilter* invf = (filters::InvertFilter*)list_->getFilter(DEFAULT_INVERT_NAME);
    if(invf->isActive()){
        bsf->setReference(invf->getDestination());
    } else {
        bsf->setReference(list_->getFilter(DEFAULT_GRAY_NAME)->getDestination());
    }
}

void MainWindow::invertImage(){
    list_->getFilter(DEFAULT_INVERT_NAME)->setActive(ui->invert_checkbox->isChecked());
}

void MainWindow::switchCamera(){
    GLWidget* sender = qobject_cast<GLWidget*>(QObject::sender());

    capture_ = sender->getCamera();
    output_capture_->setCamera(capture_);
    ui->camera_config_btn->setEnabled(capture_->isPS3Camera());

    screen_->setCamera(capture_);
    output_image_ = capture_->getFrame();
}

void MainWindow::initCamerasTab(){
    std::vector<std::wstring> devicesPath = dsutil::getDevices();

    for(int i = 0; i < devicesPath.size(); ++i){
        Camera* camera = NULL;
        std::string name(devicesPath[i].begin(), devicesPath[i].end());
        bool ps3 = !name.compare(PS3_CAMERA_NAME);
        if(ps3 && CLEyeGetCameraCount() > 0){
            camera = new PS3CameraCapture(i);
        } else if(CLEyeGetCameraCount() > 0) {
            camera = new CameraCapture(i, name);
        }

        if(camera && camera->isCamera()){
            if(ps3)
                cameras_.insert(cameras_.begin(), camera);
            else
                cameras_.push_back(camera);

            GLWidget* cam_widget = new GLWidget(camera);
            cam_widget->setMaximumHeight(200);

            ui->camera_list_layout->addWidget(cam_widget);

            connect(timer_camera_, SIGNAL(timeout()), cam_widget, SLOT(setFrame()));
            connect(cam_widget, SIGNAL(clicked()), this, SLOT(switchCamera()));
        } else {
            delete camera;
        }
    }
    if(cameras_.size() > 0 && cameras_[0]->isPS3Camera()) 
        ui->camera_config_btn->setEnabled(true);
}

void MainWindow::initGLWidgets(){
    output_capture_ = new GLWidget(capture_);
    ui->camera_output_layout->addWidget(output_capture_);
    connect(timer_camera_, SIGNAL(timeout()), output_capture_, SLOT(setFrame()));

    camera_gl_widget = new GLWidget();
    output_gl_widget = new GLWidget();
    ui->camera_layout->addWidget(camera_gl_widget);
    ui->output_layout->addWidget(output_gl_widget);
    connect(timer_filters_, SIGNAL(timeout()), this, SLOT(getOutput()));
}

void MainWindow::openConfigDialog(){
    ConfigDialog* dialog = new ConfigDialog(this);
    dialog->exec();
}

PS3Settings MainWindow::getSettings(){
    return (dynamic_cast<PS3CameraCapture*>(capture_))->getSettings();
}

void MainWindow::setSettings(PS3Settings value){
    (dynamic_cast<PS3CameraCapture*>(capture_))->setSettings(value);
}

void MainWindow::saveConfig(){
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save configuration"), "",
        tr("YAML (*.yml);;All Files (*)"));

    if (fileName.isEmpty()) return;
    else screen_->saveConfig(fileName.toUtf8().constData());
}

void MainWindow::loadConfig(){
    int i;
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Load configuration"), "",
        tr("YAML (*.yml);;All Files (*)"));
    Camera* tmp;
        
    if (fileName.isEmpty()) 
        return;
    
    tmp = screen_->loadConfig(fileName.toUtf8().constData());
    for(i = 0; i < cameras_.size(); ++i){
        if(!cameras_[i]->getName().compare(tmp->getName())){
            capture_ = cameras_[i];
            if(capture_->isPS3Camera()){
                (dynamic_cast<PS3CameraCapture*>(capture_))->setSettings((dynamic_cast<PS3CameraCapture*>(tmp))->getSettings());
            }
            break;
        }
    }

    //there is no such a camera
    if(i >= cameras_.size())
        return; 
    
    delete tmp;

    emit updateSlider(APERTURE_NAME, list_->getFilter(DEFAULT_BLUR_NAME)->getParameter(APERTURE_NAME));
    emit updateSlider(LEVEL_NAME, list_->getFilter(DEFAULT_HIGHPASS_NAME)->getParameter(LEVEL_NAME));
    emit updateSlider(THRESHOLD_NAME, list_->getFilter(DEFAULT_THRESHOLD_NAME)->getParameter(THRESHOLD_NAME));

    emit updateCheckbox(DEFAULT_BLUR_NAME, list_->getFilter(DEFAULT_BLUR_NAME)->isActive());
    emit updateCheckbox(DEFAULT_HIGHPASS_NAME, list_->getFilter(DEFAULT_HIGHPASS_NAME)->isActive());
    emit updateCheckbox(DEFAULT_THRESHOLD_NAME, list_->getFilter(DEFAULT_THRESHOLD_NAME)->isActive());

    emit updateSlider("min_blob", controller_->getMinBlobSize());
    emit updateSlider("max_blob", controller_->getMaxBlobSize());

    ui->invert_checkbox->setChecked(list_->getFilter(DEFAULT_INVERT_NAME)->isActive());

    output_capture_->setCamera(capture_);
    ui->camera_config_btn->setEnabled(capture_->isPS3Camera());

    screen_->setCamera(capture_);
    output_image_ = capture_->getFrame();
}

void MainWindow::calibrate(){
    screen_->attachListener((TListener*)calibration_);
    calibration_->initialize(output_image_);
}

void MainWindow::testCalibration(){
    screen_->attachListener((TListener*)tester_);
    tester_->initialize();
}

