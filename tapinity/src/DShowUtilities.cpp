//modified DirectShow AMCap example source code

#include <vector>
#include <dshow.h>

#include "DShowUtilities.h"

namespace dsutil {

    HRESULT EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum)
    {
        // Create the System Device Enumerator.
        ICreateDevEnum *pDevEnum;
        HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL,  
            CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pDevEnum));

        if (SUCCEEDED(hr))
        {
            // Create an enumerator for the category.
            hr = pDevEnum->CreateClassEnumerator(category, ppEnum, 0);
            if (hr == S_FALSE)
            {
                hr = VFW_E_NOT_FOUND;  // The category is empty. Treat as an error.
            }
            pDevEnum->Release();
        }
        return hr;
    }

    std::vector<std::wstring> DisplayDeviceInformation(IEnumMoniker *pEnum)
    {
        std::vector<std::wstring> devicesPath;
        IMoniker *pMoniker = NULL;

        while (pEnum->Next(1, &pMoniker, NULL) == S_OK)
        {
            IPropertyBag *pPropBag;
            HRESULT hr = pMoniker->BindToStorage(0, 0, IID_PPV_ARGS(&pPropBag));
            if (FAILED(hr))
            {
                pMoniker->Release();
                continue;  
            } 

            VARIANT var;
            VariantInit(&var);

            // Get description or friendly name.
            hr = pPropBag->Read(L"FriendlyName", &var, 0);
            if (SUCCEEDED(hr))
            {
                std::wstring name(var.bstrVal, SysStringLen(var.bstrVal));
                devicesPath.push_back(name); 
                SysFreeString(var.bstrVal);
            }

            VariantClear(&var);

            pPropBag->Release();
            pMoniker->Release();
        }
        return devicesPath;
    }

    std::vector<std::wstring> getDevices()
    {
        std::vector<std::wstring> devicesPath;
        HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
        if (SUCCEEDED(hr))
        {
            IEnumMoniker *pEnum;

            hr = EnumerateDevices(CLSID_VideoInputDeviceCategory, &pEnum);
            if (SUCCEEDED(hr))
            {
                devicesPath = DisplayDeviceInformation(pEnum);
                pEnum->Release();
            }
            CoUninitialize();
        }
        return devicesPath;
    }
}