#include "ConfigDialog.h"

ConfigDialog::ConfigDialog(QWidget* parent)
    : QDialog(parent), ui(new Ui::ConfigDialog) {
        ui->setupUi(this);
        settings_ = qobject_cast<MainWindow *>(parent)->getSettings();

        initSettings();

        connect(ui->exposure_slider, SIGNAL(valueChanged(int)), this, SLOT(setExposure(int)));
        connect(ui->gain_slider, SIGNAL(valueChanged(int)), this, SLOT(setGain(int)));
        connect(ui->auto_exposure_cbox, SIGNAL(stateChanged(int)), this, SLOT(setAutoExposure(int)));
        connect(ui->auto_gain_cbox, SIGNAL(stateChanged(int)), this, SLOT(setAutoGain(int)));

        connect(ui->auto_exposure_cbox, SIGNAL(stateChanged(int)), ui->exposure_slider, SLOT(reverseEnabled(int)));
        connect(ui->auto_gain_cbox, SIGNAL(stateChanged(int)), ui->gain_slider, SLOT(reverseEnabled(int)));
}

ConfigDialog::~ConfigDialog() {
    delete ui;
}

void ConfigDialog::setGain(int value){
    settings_.gain = value;
    qobject_cast<MainWindow *>(parentWidget())->setSettings(settings_);
}

void ConfigDialog::setExposure(int value){
    settings_.exposure = value;
    qobject_cast<MainWindow *>(parentWidget())->setSettings(settings_);
}

void ConfigDialog::setAutoGain(int value){
    settings_.auto_gain = value;
    qobject_cast<MainWindow *>(parentWidget())->setSettings(settings_);
}

void ConfigDialog::setAutoExposure(int value){
    settings_.auto_exposure = value;
    qobject_cast<MainWindow *>(parentWidget())->setSettings(settings_);
}

void ConfigDialog::initSettings(){
    ui->exposure_slider->setValue(settings_.exposure);
    ui->exposure_slider->setEnabled(!settings_.auto_exposure);
    ui->auto_exposure_cbox->setChecked(settings_.auto_exposure);
    ui->gain_slider->setValue(settings_.gain);
    ui->gain_slider->setEnabled(!settings_.auto_gain);
    ui->auto_gain_cbox->setChecked(settings_.auto_gain);
}