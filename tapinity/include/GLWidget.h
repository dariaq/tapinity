#ifndef _GLWIDGET_H
#define _GLWIDGET_H

#include <QtOpenGL>

#include <cv.h>
#include <highgui.h>

#include "Tapinity.h"
#include "FiltersList.h"

using namespace tapinity;

class GLWidget: public QGLWidget{
    Q_OBJECT;

private:
    IplImage* m_image;
    filters::TFilter* filter_;
    Camera* camera_;
    int width_, height_;
GLuint texture_;
public:
    GLWidget(QWidget* parent=NULL);
    GLWidget(filters::TFilter* filter, QWidget* parent = NULL);
    GLWidget(Camera* camera, QWidget* parent = NULL);
    ~GLWidget();
    Camera* getCamera() { return camera_; };
    void setCamera(Camera* camera) { camera_ = camera; };

protected:
    void initializeGL();
    void paintGL();
    void mousePressEvent(QMouseEvent*);

    public slots:
        void setFrame(IplImage* img);
        void setFrame();

signals:
        void clicked();
};
#endif // _GLWIDGET_H
