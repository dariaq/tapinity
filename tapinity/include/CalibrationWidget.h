#ifndef _CALIBRATIONWIDGET_H
#define _CALIBRATIONWIDGET_H

#include <QtOpenGL>

#include <cv.h>
#include <highgui.h>

#include "Tapinity.h"
#include "TListener.h"
#include "CalibrationUnit.h"

using namespace tapinity;

class CalibrationWidget : public QGLWidget, TListener{
    Q_OBJECT
public:
    CalibrationWidget(TScreen* screen, CalibrationUnit* calibration, QWidget* parent=NULL) : QGLWidget(parent) {
        calibration_ = calibration;
        screen_=screen;
    }
    ~CalibrationWidget();

    virtual void touchDown(TEvent* e);
    virtual void touchUp(TEvent* e);
    virtual void touchMove(TEvent* e);

    void initialize(IplImage* image);

protected:
    void initializeGL();
    void paintGL();
    void keyPressEvent(QKeyEvent *k);

private:
    void drawFinger(float x1, float y1, float s, float r, float g, float b, float ang);

    CalibrationUnit* calibration_;
    TScreen* screen_;
};

#endif