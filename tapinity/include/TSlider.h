#ifndef _TSLIDER_H
#define _TSLIDER_H

#include <QSlider>

class TSlider: public QSlider{
    Q_OBJECT;

public:
    TSlider(QWidget * parent = 0 ) : QSlider(parent) {};
    TSlider(Qt::Orientation orientation, QWidget * parent = 0) : QSlider(orientation, parent) {};
    ~TSlider() {};

    public slots:
        void reverseEnabled(int value) { this->setEnabled(value ? false : true); };
};

#endif // _TSLIDER_H
