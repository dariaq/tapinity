#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H

#define DEFAULT_WIDTH 434
#define DEFAULT_HEIGHT 330

#include <QtGui/QMainWindow>
#include <cv.h>
#include <highgui.h>

#include <vector>

#include "DShowUtilities.h"

#include "Tapinity.h"

#include "GLWidget.h"
#include "ComponentSlider.h"
#include "TSlider.h"
#include "FilterCheckbox.h"
#include "ConfigDialog.h"
#include "BlobController.h"
#include "CalibrationWidget.h"
#include "Tester.h"

#include "ui_TapinityGUI.h"

namespace tf = ::tapinity::filters;
namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();

    PS3Settings getSettings();
    void setSettings(PS3Settings settings);  

private:
    void initFiltersFrames();
    void initFiltersSliders();
    void initBlobSliders();
    void initCamerasTab();
    void initGLWidgets();
    void setTimers(QTimer*, QTimer*);

    Ui::MainWindow* ui;

    GLWidget* output_capture_;
    GLWidget* camera_gl_widget;
    GLWidget* output_gl_widget;

    CalibrationWidget* calibration_; 
    TesterWidget* tester_;

    QTimer* timer_filters_;
    QTimer* timer_camera_;

    std::vector<GLWidget*> filters_widgets_;

    float fps_;

    TScreen* screen_;
    Camera* capture_;
    IplImage* output_image_;
    IplImage* calculations_image_;

    tf::FilterList* list_;
    BlobController* controller_;

    std::vector<Camera*> cameras_;


    private slots:
        void getOutput();
        void substractBackground();
        void invertImage();
        void switchCamera();
        void changeTimers(int idx);
        void openConfigDialog();
        void saveConfig();
        void loadConfig();
        void calibrate();
        void testCalibration();
       
signals:
        void updateSlider(std::string name, int value);
        void updateCheckbox(std::string name, bool value);

};


#endif // _MAINWINDOW_H
