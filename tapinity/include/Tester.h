#ifndef _TESTER_H
#define _TESTER_H

#include <QtOpenGL>
#include <qtimer.h>
#include <cv.h>
#include <highgui.h>

#include "Tapinity.h"

using namespace tapinity;

class TesterWidget: public QGLWidget, TListener{
    Q_OBJECT;

public:
    TesterWidget(TScreen* screen, QWidget* parent=NULL);
    ~TesterWidget();

    virtual void touchDown(TEvent* e);
    virtual void touchUp(TEvent* e);
    virtual void touchMove(TEvent* e);

    void initialize();
    void drawFinger(float x1, float y1, float s, float r, float g, float b, float ang);
    void debugInfo();
    void drawTriangle(CvPoint2D32f p0, CvPoint2D32f p1, CvPoint2D32f p2);

protected:
    void initializeGL();
    void paintGL();
    void keyPressEvent(QKeyEvent *k);

    std::vector<TData> to_draw;
    TScreen* screen_;
    QTimer* timer_;
};
#endif
