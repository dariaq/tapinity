#ifndef _COMPONENTSLIDER_H
#define _COMPONENTSLIDER_H

#include <QtOpenGL>

#include <cv.h>
#include <highgui.h>

#include "Tapinity.h"

using namespace tapinity;

class ComponentSlider: public QSlider{
    Q_OBJECT;

    string name_;
    filters::TFilter* filter_;

public:
    ComponentSlider(Qt::Orientation orientation, filters::TFilter* filter, std::string name) : QSlider(orientation, NULL), filter_(filter), name_(name){
        setValue(filter_->getParameter(name));
    };

    ComponentSlider(Qt::Orientation orientation, std::string name) : QSlider(orientation, NULL), name_(name){ };
    ~ComponentSlider() {};

    public slots:
        void setFilter(int value){ filter_->setParameter(name_, value); };
        void setSlider(std::string name, int value){ if(!name_.compare(name)) setValue(value); };
        int getFilter(std::string param){ return filter_->getParameter(param); }
};

#endif // _COMPONENTSLIDER_H
