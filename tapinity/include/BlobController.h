#ifndef _BLOBCONTROLLER_H
#define _BLOBCONTROLLER_H

#include <QObject>

#include <vector>
#include <cv.h>
#include <highgui.h>
#include "Tapinity.h"

namespace tb = ::tapinity::blobs;

class BlobController: public QObject{
    Q_OBJECT;
public:
    BlobController(tb::BlobTracker* bf);
    ~BlobController();

    void drawBlob(IplImage* image, tb::Blob b,int n, bool boundingBox);
    void drawBlobMovement(IplImage* image, tb::Blob b1, tb::Blob b2);
    void frameAction(IplImage* calculations_image, IplImage* draw_image, bool boundingBoxes=false); //finds blobs in current frame, tracks them, draws (+draws boundingBoxes)

    public slots:
        void setMinBlobSize(int value);
        void setMaxBlobSize(int value);
        int getMinBlobSize();
        int getMaxBlobSize();


private:
    tb::ContoursFinder* finder_;
    tb::BlobTracker* tracker_;

};

#endif