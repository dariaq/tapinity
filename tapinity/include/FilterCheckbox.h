#ifndef _FILTERCHECKBOX_H
#define _FILTERCHECKBOX_H

#include <QtOpenGL>

#include <cv.h>
#include <highgui.h>

#include "Tapinity.h"

using namespace tapinity;

class FilterCheckbox: public QCheckBox {
    Q_OBJECT;

    std::string name_;
    filters::TFilter* filter_;

public:
    FilterCheckbox(filters::TFilter* filter, std::string name) : QCheckBox(NULL), filter_(filter), name_(name){
        setChecked(true);
    };
    ~FilterCheckbox() {};

    public slots:
        void setActive() { filter_->setActive(isChecked()); };
        void setActive(std::string name, bool value){ 
            if(!name_.compare(name)) 
                setChecked(value); 
        };
        bool getFilterActive(){ return filter_->isActive(); }
};
#endif // _FILTERCHECKBOX_H
