#ifndef _CONFIGDIALOG_H
#define _CONFIGDIALOG_H

#include <QtGui/QDialog>

#include "PS3CameraCapture.h"

#include "MainWindow.h"
#include "TSlider.h"
#include "ui_ConfigDialog.h"

namespace Ui {
    class ConfigDialog;
}

class ConfigDialog : public QDialog {
    Q_OBJECT

public:
    ConfigDialog(QWidget* parent = 0);
    ~ConfigDialog();

    private slots:
        void setGain(int);
        void setAutoGain(int);
        void setExposure(int);
        void setAutoExposure(int);

private:
    Ui::ConfigDialog* ui;
    tapinity::PS3Settings settings_;

    void initSettings();
};

#endif // _CONFIGDIALOG_H