//DirectShow AMCap example source code

#pragma comment(lib,"Strmiids.lib") 

#ifndef _DSHOWUTILITIES_H
#define _DSHOWUTILITIES_H

#include <vector>
#include <dshow.h>

namespace dsutil {
    HRESULT EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum);
    std::vector<std::wstring> DisplayDeviceInformation(IEnumMoniker *pEnum);
    std::vector<std::wstring> getDevices();
}

#endif // _DSHOWUTILITIES_H